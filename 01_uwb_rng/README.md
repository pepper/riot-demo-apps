UWB Two Way Ranging
===================

This app illustrates the ranging features of the uwb-core (and uwb-dwm1001)
pkg running on the dwm1001 chip.

The application will provide a series of shell commands to perform
two-way ranging requests (ALOHA) between nodes.

A detailed walkthrough will leverage this application to perform
range requests on the [FIT IoT-LAB](https://www.iot-lab.info/)
testbed, this can also be ran on local hardware if available.

Table of contents:

- [Prerequisites][prerequisites]
- [Walkthrough][walkthrough]
- [About This Application][about]

### Prerequisites
[prerequisites]: #rerequisites

1. Install iotlab-cli tools

    - pip install iotlab-cli

2. Create an iotlab account

    - https://www.iot-lab.info/testbed/signup

3. Authenticate:

```shell
$ iotlab-auth --user <username> --password <password>
```

4. OPTIONAL: if using the [parse_plot.py](plot_demo/parse_plot.py)
script then install the required python libraries:

    - pip install -r plot_demo/requirements.txt

### Walkthrough
[walkthrough]: #Walkthrough

In this exercise, you will need two or more `dwm1001` boards.

UWB support in RIOT is provided by the [external package uwb-core](https://github.com/Decawave/uwb-core) which has been ported to RIOT and provides many UWB ranging features.

In this application, you will:
- build an application with shell commands allowing to a device to perform
  TWR requests against another
- retrieve the scan output and store it in a file
- plot the results

#### Launch an Experiment

1. Submit a 60 minutes experiment with 2 dwm1001 boards:

```shell
$ iotlab-experiment submit -n "riot-ble-scan" -d 60 -l 2,archi=dwm1001:dw1000+site=saclay
```

2. Wait for the experiment to be in the Running state:

```shell
$ iotlab-experiment wait --timeout 30 --cancel-on-timeout
```

**Note:** If the command above returns the message `Timeout reached, cancelling experiment <exp_id>`, try to re-submit your experiment later.

3. Get the experiment nodes list:

``` bash
$ iotlab-experiment get --nodes
{
    "items": [
        ...
        {
            "archi": "dwm1001:dw1000",
            "camera": "0",
            "mobile": "0",
            "mobility_type": " ",
            "network_address": "dwm1001-2.saclay.iot-lab.info",
            "production": "YES",
            "site": "saclay",
            "state": "Alive",
            "uid": " ",
            "x": "4.42",
            "y": "68.3",
            "z": "6"
        },
        ...
    ]
}
```

Take note of each node's `"network_address"`, we will use this to
connect to specific devices. The `"network_address"` follows the following
structure `<node-name>-<id>.saclay.iot-lab.info`. For the rest of this
walkthrough every time you see `<node>` replace with the
matching `"network_address"`

### Play with the RIOT firmware

Let's start by connecting to the first board which will be used to
send range requests.

1. Open a serial terminal on the first board. Note that it will output
nothing by default, you have to enter commands (see the next steps).

```shell
$ TERMLOG=$(pwd)/plot_demo/scan_raw.log make IOTLAB_NODE=<node> term
```
_Note:_  in the command above, we use the `TERMLOG` variable with a file
path. This is a RIOT build system trick that will dump all RIOT shell
output to a log file. We will use this file later to parse the range
request parameters.


2. In the RIOT shell opened by the previous commands, you can check the
available commands:

```shell
> help
help
Command              Description
---------------------------------------
twr                  Two Way Ranging CLI
ifconfig             Configure network interfaces
reboot               Reboot the node
version              Prints current RIOT_VERSION
pm                   interact with layered PM subsystem
ps                   Prints information about running threads.
> twr
twr
Usage:
  twr start <short_addr> <time [ms]>
   - short_addr: short addres to send ranging request
   - time: send out range request for <time> ms
  twr stop: stop sending ranging requests
  twr set <alg|role> <value>: set parameters
  twr get <alg|role>: get parameters
```

Keep this terminal open, we will come back to it later to send the requests

3. Open a serial terminal on the remaining board(s):

```shell
$ make IOTLAB_NODE=<node> term
```

4. Configure the board as an anchor and retrieve its short address. Once
an anchor is configured the device will be constantly listening for range
requests

```shell
  > twr set role anchor
  > ifconfig
    ifconfig
    Iface  3        HWaddr: 03:13  NID: CA:DE

                    Long HWaddr: 03:13:D1:CA:88:C1:02:04
```

Here the sort address is `03:13`, take note of this value.

5. Now go back to the first RIOT shell and start sending range requests
to the anchor of address `03:13` by using the `twr start` command. Here it
will send range requests for 1s (1000ms). Range requests logs will show up
in the nodes terminal.

```shell
> twr start 03:13 1000
  twr start 03:13 1000
Setting timeout to 1000
Start ranging
{"utime": 27271562, "uid": 17581, "ouid": 4867, "raz": [0.4408, 0.0, 0.0], "tof": 93.9943, "rssi": -80.6607, "los": 1}
...
{"utime": 27324830, "uid": 17581, "ouid": 4867, "raz": [0.4449, 0.0, 0.0], "tof": 94.8662, "rssi": -80.7528, "los": 1}
```

### Free up the resources

You are done with the experiment, so stop your experiment to free up the
devices:

``` bash
$ iotlab-experiment stop
```

### Process the log file

If everything went well, you should have a [scan_raw.log](scan_raw.log)
in the `plot_demo` directory.

``` bash
    $ head -n 10 plot_demo/scan_raw.log
```

As you can see, this file contains all the output cached by the scanning
node, including the shell input commands.

To parse the output you can use the helper script in
[plot_demo/parser.py](plot_demo/parser.py)

```shell
$ python plot_demo/parse_plot.py plot_demo/scan_raw.log
```

You will get some figures similar to:

![](plot_demo/pics/Figure_1.png)

![](plot_demo/pics/Figure_2.png)

![](plot_demo/pics/Figure_3.png)

![](plot_demo/pics/Figure_4.png)

### About This Application
[about]: #About-This-Application

The application provides shell commands to perform TWR range Requests:

- `twr start <short_addr> <time [ms]>`: start ranging activity to
  `<short_addr>` for `<time>` (if omitted run until stopped)
- `twr stop`: start ranging activity
- `twr set alg`: set ranging algorithm `<ss|ss-ext|ss-ack|ds|ds-ack>`
- `twr set alg`: set role `<anchor|tag>`
- `twr get <alg|role>`: show parameter values
- `ifconfig`: network interface information

#### `twr start` & `twr stop` cmd

```shell

Command              Description
---------------------------------------
twr                  Two Way Ranging CLI

usage: twr start <short_addr> <time [ms]>
   - short_addr: short address to send ranging request
   - time: send out range request for <time> ms

usage: twr stop: stop sending ranging requests
```

This commands allows to perform range requests to the device with short
address `<short_addr>`. Range requests will be performed continuously
until `twr stop` command is issued, or until `<time>` time has elapsed
(this parameter an be omitted).

Usage Examples:

- send range requests for 1s

```shell
> twr start 03:13 1000
  twr start 03:13 1000
Setting timeout to 1000
Start ranging
{"utime": 27271562, "uid": 17581, "ouid": 4867, "raz": [0.4408, 0.0, 0.0], "tof": 93.9943, "rssi": -80.6607, "los": 1}
...
{"utime": 27324830, "uid": 17581, "ouid": 4867, "raz": [0.4449, 0.0, 0.0], "tof": 94.8662, "rssi": -80.7528, "los": 1}
```

- send range request until `twr stop` is issued

```shell
> twr start 03:13
  twr start 03:13
Start ranging
{"utime": 27271562, "uid": 17581, "ouid": 4867, "raz": [0.4408, 0.0, 0.0], "tof": 93.9943, "rssi": -80.6607, "los": 1}
...
{"utime": 27324830, "uid": 17581, "ouid": 4867, "raz": [0.4449, 0.0, 0.0], "tof": 94.8662, "rssi": -80.7528, "los": 1}
> twr stop
>
```

#### `twr set` and `twr get` cmd

```shell
Command              Description
---------------------------------------
twr                  Two Way Ranging CLI

usage: twr set alg <ss|ss-ext|ss-ack|ds|ds-ack>
usage: twr set role <anchor|tag>

usage: twr get alg
usage: twr get role
```

This commands allow to set default configurations on the devices. It
allows to change the TWR protocol used in the request as well as changing
the device role.

Usage Examples:

- set/get role
```
> twr get role
twr role 'tag'
> twr set role anchor
> twr get role
twr role 'anchor'
```

- set/get algorithm
```
> twr get alg
twr algorithm 'ss'
> twr set alg ss-ack
> twr get alg
twr algorithm 'ss-ack'
```

#### `ifconfig` cmd

```shell
Command              Description
---------------------------------------
ifconfig             Configure network interfaces
```

This command shows network information, namely the device short address,
long address and the PANID.

```shell
  > ifconfig
    ifconfig
    Iface  3        HWaddr: 03:13  NID: CA:DE

                    Long HWaddr: 03:13:D1:CA:88:C1:02:04
```

#### Compile time defaults

At compile time some default are set for:
 - DW1000_ROLE: UWB_ROLE_TAG or UWB_ROLE_ANCHOR
 - ANCHOR_ADDRESS_DEFAULT: default address where range request will be sent
 - RANGE_REQUEST_PERIOD_US: range request period in us
 - UWB_TWR_ALGORITHM_DEFAULT: default ranging algorithm to use in each
   request:
    - TWR_ALGORITHM_SS
    - TWR_ALGORITHM_SS_ACK
    - TWR_ALGORITHM_SS_EXT
    - TWR_ALGORITHM_DS
    - TWR_ALGORITHM_DS_ACK

These values can be changed at compile time by setting it through the
environment or directly in the application `Makefile`.
