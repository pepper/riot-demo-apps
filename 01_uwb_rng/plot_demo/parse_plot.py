#!/usr/bin/env python3

import argparse
import json
import logging
from dataclasses import dataclass, asdict
from typing import List, Dict
import sys
import re

from dacite import from_dict
import matplotlib.pyplot as plt

LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
LOG_LEVELS = ('debug', 'info', 'warning', 'error', 'fatal', 'critical')
LOGGER = logging.getLogger("parser")

PARSER = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
PARSER.add_argument('files', nargs='+', help='Range measurements files')
PARSER.add_argument('--loglevel', choices=LOG_LEVELS, default='info',
                    help='Python logger log level')
PARSER.add_argument('--no-plot', action="store_false",
                    help='Do not plot')

@dataclass
class TwrData:
    utime: int
    uid: int
    ouid: int
    raz: List[float]
    rssi: float
    los: float
    tof: float

    def euid(self) -> str:
        eui = self.uid
        return '{:0>4X}'.format(((eui << 8) | (eui >> 8)) & 0xFFFF)

    def dest_euid(self) -> str:
        eui = self.ouid
        return '{:0>4X}'.format(((eui << 8) | (eui >> 8)) & 0xFFFF)

    def time_s(self) -> float:
        return (self.utime / 1e6)

    def from_json_str(json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=TwrData, data=json_dict)


def parse_twr_captures(logfile):
    captures = dict()

    def append_twr_data(node: TwrData):
        euid = node.dest_euid()
        if euid not in captures:
            captures[euid] = {
            'time': [node.time_s()],
            'raz': [node.raz[0]],
            'rssi': [node.rssi],
            'los': [node.los],
            }
        captures[euid]['time'].append(node.time_s())
        captures[euid]['raz'].append(node.raz[0])
        captures[euid]['rssi'].append(node.rssi)
        captures[euid]['los'].append(node.los)

    with open(logfile, "r") as file:
        for line in file:
            try:
                data = TwrData.from_json_str(line)
                append_twr_data(data)
            except ValueError:
                LOGGER.error("JSONDecodeError on: '{}'".format(line))
    return captures


def print_stats(twr_data: Dict[str, int]):
    LOGGER.info(f'Parsed {len(twr_data)} nodes: {list(twr_data.keys())}')
    for node in twr_data.keys():
        samples = len(twr_data[node]['time'])
        LOGGER.info(f'\t{node}: {samples} samples')


def plot_rssi_timeseries(data: Dict[str, int]):
    n = len(data)
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True)
    for ax, node in zip(fig.axes, data):
        ax.plot(data[node]['rssi'], label=node)
        ax.legend()
        ax.set_ylabel('RSSI (dBm)')
        ax.grid(True)
        ax.set_xlabel('sample')
    fig.suptitle('RSSI timeseries')


def plot_rss_histograms(data: Dict[str, int]):
    n = len(data)
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True)
    for ax, node in zip(fig.axes, data):
        ax.hist(data[node]['rssi'], label=node)
        ax.legend()
        ax.set_ylabel('frequency')
        ax.set_xlabel('RSSI (dBm')
    fig.suptitle('RSSI distributions')


def plot_rss_histograms_single(data: Dict[str, int]):
    fig, axs = plt.subplots(sharey=True, sharex=True, tight_layout=True)
    for node in data:
        axs.hist(data[node]['rssi'], label=node)
    axs.legend()
    axs.set_ylabel('frequency')
    axs.set_xlabel('RSSI (dBm')
    fig.suptitle('RSSI distributions')


def plot_range_timeseries(data: Dict[str, int]):
    n = len(data)
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True)
    for ax, node in zip(fig.axes, data):
        ax.plot(data[node]['raz'], label=node)
        ax.legend()
        ax.set_ylabel('Range (m)')
        ax.grid(True)
        ax.set_xlabel('sample')
    fig.suptitle('Range timeseries')


def plot_range_histograms(data: Dict[str, int]):
    n = len(data)
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True)
    for ax, node in zip(fig.axes, data):
        ax.hist(data[node]['raz'], label=node)
        ax.legend()
        ax.set_ylabel('frequency')
        ax.set_xlabel('Range (m)')
    fig.suptitle('Range distributions')


def plot_range_histograms_single(data: Dict[str, int]):
    fig, axs = plt.subplots(sharey=True, sharex=True, tight_layout=True)
    for node in data:
        axs.hist(data[node]['raz'], label=node)
    axs.legend()
    axs.set_ylabel('frequency')
    axs.set_xlabel('Range (m)')
    fig.suptitle('Range distributions')


def main(args=None):
    args = PARSER.parse_args()

    # Setup logger
    if args.loglevel:
        loglevel = logging.getLevelName(args.loglevel.upper())
        LOGGER.setLevel(loglevel)

    LOGGER.addHandler(LOG_HANDLER)
    LOGGER.propagate = False

    logfiles = args.files
    no_plot = args.no_plot

    captures = dict()
    for logfile in logfiles:
        capture = parse_twr_captures(logfile)
        print_stats(capture)
        captures.update(capture)

    if not no_plot:
        plot_rssi_timeseries(captures)
        plot_rss_histograms(captures)
        plot_rss_histograms_single(captures)
        plot_range_timeseries(captures)
        plot_range_histograms(captures)
        plot_range_histograms_single(captures)
        plt.show(block=False)
        input('Press enter to exit')


if __name__ == '__main__':
    main()
