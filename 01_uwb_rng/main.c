/*
 * Copyright (C) 2020 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Two way ranging activity example
 *
 * @author      Francisco Molina <francois-xavier.molina@inria.fr>
 *
 * @}
 */

#include <stdio.h>

#include "shell.h"
#include "shell_commands.h"

#include "timex.h"
#include "twr_range_activity.h"

extern int _twr_handler(int argc, char **argv);
extern int _twr_ifconfig(int argc, char **argv);

twr_rng_activity_t twr_rng_activity;

static const shell_command_t shell_commands[] = {
    { "twr", "Two Way Ranging CLI", _twr_handler },
    { "ifconfig", "Configure network interfaces", _twr_ifconfig},
    { NULL, NULL, NULL }
};

int main(void)
{
    puts("twr ranging activity example");

    /* setup default activity configuration */
    twr_rng_activity.algo = UWB_TWR_ALGORITHM_DEFAULT;
    twr_rng_activity.dest.u16 = ANCHOR_ADDRESS_DEFAULT;
    twr_rng_activity.period = RANGE_REQUEST_PERIOD_US;
    twr_rng_activity.run = false;
    /* UINT32_MAX for timeout never */
    twr_rng_activity.timeout = UINT32_MAX;

    /* start ranging activity based on default configuration */
    twr_rng_activity_init(&twr_rng_activity);

    /* define buffer to be used by the shell */
    char line_buf[SHELL_DEFAULT_BUFSIZE];
    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);
    return 1;
}
