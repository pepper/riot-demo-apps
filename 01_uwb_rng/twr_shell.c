/*
 * Copyright (C) 2020 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 *
 * @author      Francisco Molina <francois-xavier.molina@inria.fr>
 *
 * @}
 */

#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "net/ieee802154.h"
#include "net/netif.h"
#include "shell_commands.h"
#include "shell.h"

#include "uwb/uwb.h"
#include "twr_range_activity.h"

#define IEEE802154_LONG_ADDRESS_LEN_STR_MAX \
    (sizeof("00:00:00:00:00:00:00:00"))

extern twr_rng_activity_t twr_rng_activity;

int _twr_ifconfig(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    char addr_str[IEEE802154_LONG_ADDRESS_LEN_STR_MAX];
    struct uwb_dev *udev = uwb_dev_idx_lookup(0);

    printf("Iface  %d", udev->task_str.pid);

    printf("\tHWaddr: %s  ",
           netif_addr_to_str((const uint8_t *)&udev->uid, sizeof(udev->uid),
                             addr_str));

    printf("NID: %s\n\n",
           netif_addr_to_str((const uint8_t *)&udev->pan_id, sizeof(udev->pan_id),
                             addr_str));

    printf("\t\tLong HWaddr: %s\n",
           netif_addr_to_str((const uint8_t *)&udev->euid, sizeof(udev->euid),
                             addr_str));

    return 0;
}

static void _print_usage(void)
{
    puts("Usage:");
    puts("\ttwr start <short_addr> <time [ms]>");
    puts("\t - short_addr: short addres to send ranging request");
    puts("\t - time: send out range request for <time> ms");
    puts("\ttwr stop: stop sending ranging requests");
    puts("\ttwr set <alg|role> <value>: set parameters");
    puts("\ttwr get <alg|role>: get parameters");
}

int _twr_handler(int argc, char **argv)
{
    if (argc < 2) {
        _print_usage();
        return -1;
    }

    if (!strcmp(argv[1], "set")) {
        if (argc > 2) {
            if (!strcmp(argv[2], "alg")) {
                if (argc > 3) {
                    if (!strcmp(argv[3], "ss")) {
                        twr_rng_activity_set_algo(TWR_ALGORITHM_SS);
                    }
                    else if (!strcmp(argv[3], "ss-ack")) {
                        twr_rng_activity_set_algo(TWR_ALGORITHM_SS_ACK);
                    }
                    else if (!strcmp(argv[3], "ss-ext")) {
                        twr_rng_activity_set_algo(TWR_ALGORITHM_SS_EXT);
                    }
                    else if (!strcmp(argv[3], "ds")) {
                        twr_rng_activity_set_algo(TWR_ALGORITHM_DS);
                    }
                    else if (!strcmp(argv[3], "ds-ack")) {
                        twr_rng_activity_set_algo(TWR_ALGORITHM_DS_ACK);
                    }
                    else {
                        printf("[ERROR]: invalid alg %s\n", argv[3]);
                        return -1;
                    }
                    printf("[OK]: twr algorithm set to '%s'\n", argv[3]);
                    return 0;
                }
                else {
                    puts("Usage:");
                    puts("\ttwr set alg <ss|ss-ext|ss-ack|ds|ds-ack>");
                    return -1;
                }
            }
        }
        if (!strcmp(argv[2], "role")) {
            if (argc > 3) {
                struct uwb_dev *udev = uwb_dev_idx_lookup(0);
                if (!strcmp(argv[3], "anchor")) {
                    twr_set_role(udev, UWB_ROLE_ANCHOR);
                }
                else if (!strcmp(argv[3], "tag")) {
                    twr_set_role(udev, UWB_ROLE_TAG);
                }
                else {
                    printf("[ERROR]: invalid role %s\n", argv[3]);
                    return -1;
                }
                return 0;
            }
            else {
                puts("Usage:");
                puts("\ttwr set role <anchor|tag>");
                return -1;
            }
        }
    }

    if (!strcmp(argv[1], "get")) {
        if (argc > 2) {
            if (!strcmp(argv[2], "alg")) {
                if (twr_rng_activity_get_algo() == TWR_ALGORITHM_SS) {
                    puts("twr algorithm 'ss'");
                }
                else if (twr_rng_activity_get_algo() == TWR_ALGORITHM_SS_ACK) {
                    puts("twr algorithm 'ss-ack'");
                }
                else if (twr_rng_activity_get_algo() == TWR_ALGORITHM_SS_EXT) {
                    puts("twr algorithm 'ss-ext'");
                }
                else if (twr_rng_activity_get_algo() == TWR_ALGORITHM_DS) {
                    puts("twr algorithm 'ds'");
                }
                else if (twr_rng_activity_get_algo() == TWR_ALGORITHM_DS_ACK) {
                    puts("twr algorithm 'ds-ack'");
                }
                return 0;
            }
            if (!strcmp(argv[2], "role")) {
                struct uwb_dev *udev = uwb_dev_idx_lookup(0);
                if (twr_get_role(udev) == UWB_ROLE_ANCHOR) {
                    puts("twr role 'anchor'");
                }
                else if (twr_get_role(udev) == UWB_ROLE_TAG) {
                    puts("twr role 'tag'");
                }
                return 0;
            }
        }
    }

    if (!strcmp(argv[1], "start")) {
        uint32_t timeout = DPL_TIMEOUT_NEVER;
        if (argc > 2) {
            /* parse destination address */
            uint8_t addr[IEEE802154_LONG_ADDRESS_LEN_STR_MAX];
            size_t addr_len = netif_addr_from_str(argv[2], addr);
            if (addr_len != 2) {
                puts("Error: unable to parse address.\n"
                     "Must be of format [0-9a-fA-F]{2}(:[0-9a-fA-F]{2})*\n"
                     "(hex pairs delimited by colons)");
                return 1;
            }
            else {
                twr_rng_activity_set_dest(addr);
            }
            if (argc > 3) {
                timeout = atoi(argv[3]);
                printf("Setting timeout to %"PRIu32"\n", timeout);
            }
        }
        printf("Start ranging\n");
        twr_rng_activity_set_timeout(timeout);
        twr_rng_activity_start();
        if(timeout != DPL_TIMEOUT_NEVER) {
            xtimer_msleep(timeout);
        }
        return 0;
    }

    if (!strcmp(argv[1], "stop")) {
        printf("Stop ranging\n");
        twr_rng_activity_stop();
        return 0;
    }

    _print_usage();
    return -1;
}
