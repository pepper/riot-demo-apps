/*
 * Copyright (C) 2020 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 *
 * @author      Francisco Molina <francois-xavier.molina@inria.fr>
 *
 * @}
 */

#ifndef TWR_RANGE_ACTIVITY_H
#define TWR_RANGE_ACTIVITY_H

#ifdef __cplusplus
extern "C" {
#endif

#include "timex.h"
#include "xtimer.h"
#include "net/ieee802154.h"
#include "net/netif.h"

#include "uwb/uwb.h"
#include "uwb/uwb_ftypes.h"
#include "dpl/dpl_callout.h"

/**
 * @brief TWR algorithms type
 */
typedef enum {
    TWR_ALGORITHM_NONE = 0,
    TWR_ALGORITHM_SS = UWB_DATA_CODE_SS_TWR,
    TWR_ALGORITHM_SS_ACK = UWB_DATA_CODE_SS_TWR_ACK,
    TWR_ALGORITHM_SS_EXT = UWB_DATA_CODE_SS_TWR_EXT,
    TWR_ALGORITHM_DS = UWB_DATA_CODE_DS_TWR,
    TWR_ALGORITHM_DS_ACK = UWB_DATA_CODE_DS_TWR_EXT,
} twr_algorithm_t;

/**
 * @brief   TWR range activity structure
 */
typedef struct {
    twr_algorithm_t algo;       /**< algorithm to use */
    network_uint16_t dest;      /**< dest short address */
    struct dpl_callout callout; /**< to schedule periodic requests */
    uint32_t timeout;           /**< timeout time for this activity [ms] */
    uint32_t period;            /**< range request period */
    bool run;                   /**< if activity is ongoing */
    xtimer_t timer;             /**< timeout timer */
} twr_rng_activity_t;

/**
 * @brief Return the configured ranging algorithm
 *
 * @param[in] udev  uwb-core device pointer
 * @param[in] role  uwb-device role
 */
void twr_set_role(struct uwb_dev *udev, uint16_t role);

/**
 * @brief Return the current uwb-device role
 *
 * @param[in] udev  uwb-core device pointer
 *
 * @return uint16_t Current uwb-device role
 */
uint16_t twr_get_role(struct uwb_dev *udev);

/**
 * @brief Return the configured ranging algorithm
*
 * @return twr_algorithm_t  Current ranging algorithm
 */
twr_algorithm_t twr_rng_activity_get_algo(void);

/**
 * @brief Sets new ranging algorithm
*
 * @param[in] algo   New ranging algorithm
 */
void twr_rng_activity_set_algo(twr_algorithm_t algo);

/**
 * @brief Copies range request destination address (short address)
 *
 * @param[out] dest Buffer to copy destination short address to
 */
void twr_rng_activity_get_dest(uint8_t* dest);

/**
 * @brief Sets range request destination address (short address)
 *
 * @param[in] dest  Pointer to short address to set as range request
 *                  destination
 */
void twr_rng_activity_set_dest(uint8_t* dest);

/**
 * @brief Returns the range request period time [us]
 *
 * @return uint32_t     Current period
 */
uint32_t twr_rng_activity_get_period(void);

/**
 * @brief Sets the range request period time [us]
 *
 * @param[in] period   Range request period time to set in [us]
 */
void twr_rng_activity_set_period(uint32_t period);

/**
 * @brief Returns the current timeout value
 *
 * @return uint32_t     Current timeout
 */
uint32_t twr_rng_activity_get_timeout(void);

/**
 * @brief Sets timeout for ranging activity
 *
 * @param[in] timeout   The timeout value in ms, UIN32_MAX or
 *                      DPL_TIMEOUT_NEVER to never timeout.
 */
void twr_rng_activity_set_timeout(uint32_t timeout);

/**
 * @brief Enables ranging activity
 */
void twr_rng_activity_start(void);

/**
 * @brief Disables ranging activity
 */
void twr_rng_activity_stop(void);

/**
 * @brief Inits the ranging activity based on the default configuration
*
 * @param[in] activity   ranging activity pointer
 */
void twr_rng_activity_init(twr_rng_activity_t* activity);

#ifdef __cplusplus
}
#endif

#endif /* TWR_RANGE_ACTIVITY_H */
