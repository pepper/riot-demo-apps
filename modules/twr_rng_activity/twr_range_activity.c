/*
 * Copyright (C) 2020 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 *
 * @author      Francisco Molina <francois-xavier.molina@inria.fr>
 *
 * @}
 */

#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "net/ieee802154.h"
#include "net/netif.h"
#include "xtimer.h"
#include "fmt.h"

#include "uwb/uwb.h"
#include "uwb_rng/uwb_rng.h"
#include "dpl/dpl.h"
#include "twr_range_activity.h"

/* offloading event for completion of range request */
static struct dpl_event _rng_req_complete_event;

/* TODO: currently we only hold a pointer to a single range activity, but
   this could be a linked list... */
static twr_rng_activity_t* _rng_activity;

void twr_set_role(struct uwb_dev *udev, uint16_t role)
{
    udev->role = role;
    if ((udev->role & UWB_ROLE_ANCHOR)) {
        /* anchor starts listening by default */
        dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);
        _rng_activity->run = true;
    }
    else {
        _rng_activity->run = false;
    }
}

uint16_t twr_get_role(struct uwb_dev *udev)
{
    return udev->role;
}

twr_algorithm_t twr_rng_activity_get_algo(void)
{
    return _rng_activity->algo;
}

void twr_rng_activity_set_algo(twr_algorithm_t algo)
{
    _rng_activity->algo = algo;
}

void twr_rng_activity_get_dest(uint8_t* dest)
{
    memcpy(dest, &_rng_activity->dest, sizeof(_rng_activity->dest));
}

void twr_rng_activity_set_dest(uint8_t* dest)
{
    memcpy(&_rng_activity->dest, dest, sizeof(_rng_activity->dest));
}

uint32_t twr_rng_activity_get_period(void)
{
    return _rng_activity->period;
}

void twr_rng_activity_set_period(uint32_t period)
{
    _rng_activity->period = period;
    dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);
}

uint32_t twr_rng_activity_get_timeout(void)
{
    return _rng_activity->timeout;
}

void twr_rng_activity_set_timeout(uint32_t timeout)
{
    _rng_activity->timeout = timeout;
    if (_rng_activity->timeout != DPL_TIMEOUT_NEVER && _rng_activity->run) {
        xtimer_set64(&_rng_activity->timer, _rng_activity->timeout * US_PER_MS);
    }
}

void twr_rng_activity_start(void)
{
    dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);
    /* Set up timeout uf needed */
    if (_rng_activity->timeout != DPL_TIMEOUT_NEVER) {
        xtimer_set64(&_rng_activity->timer, _rng_activity->timeout * US_PER_MS);
    }
    _rng_activity->run = true;
}

void twr_rng_activity_stop(void)
{
    dpl_callout_stop(&_rng_activity->callout);
    xtimer_remove(&_rng_activity->timer);
    _rng_activity->run = false;
}

/**
 * @brief Range request complete callback.
 *
 * This callback is called on completion of a range request in the
 * context of this example.
 *
 * @note This runs in ISR context, offload as much as possible to a
 * thread/event queue.
 *
 * @note The MAC extension interface is a linked-list of callbacks,
 * subsequentcallbacks on the list will be not be called if the callback
 * returns true.
 *
 * @param[in] inst  Pointer to struct uwb_dev.
 * @param[in] cbs   Pointer to struct uwb_mac_interface.
 *
 * @return true if valid recipient for the event, false otherwise
 */
static bool _complete_cb(struct uwb_dev *inst, struct uwb_mac_interface *cbs)
{
    if (inst->fctrl != FCNTL_IEEE_RANGE_16 &&
        inst->fctrl != (FCNTL_IEEE_RANGE_16 | UWB_FCTRL_ACK_REQUESTED)) {
        return false;
    }

    /* get received frame */
    struct uwb_rng_instance * rng = (struct uwb_rng_instance *)cbs->inst_ptr;
    rng->idx_current = (rng->idx) % rng->nframes;
    twr_frame_t * frame = rng->frames[rng->idx_current];

    /* calculate Local results and diagnostics */
    uint32_t utime = xtimer_now_usec();
    dpl_float32_t rssi = uwb_calc_rssi(inst, inst->rxdiag);
    dpl_float32_t fppl = uwb_calc_fppl(inst, inst->rxdiag);
    dpl_float64_t tof = uwb_rng_twr_to_tof(rng, rng->idx_current);
    dpl_float64_t range = uwb_rng_tof_to_meters(tof);
    dpl_float64_t los = DPL_FLOAT64_FROM_F32(
        uwb_estimate_los(rng->dev_inst, rssi, fppl));

    /* local buffer for to use fmt_float*/
    char buffer[32];
    /*print data as json */
    printf("{");
    printf("\"utime\": %"PRIu32", ", utime);
    printf("\"uid\": %"PRIu16", ", frame->src_address);
    printf("\"ouid\": %"PRIu16", ", frame->dst_address);
    memset(buffer, 0, sizeof(buffer));
    fmt_float(buffer, range, 4);
    /* currently we only send range value, no azimuth or zenith */
    printf("\"raz\": [%s, 0.0, 0.0], ", buffer);
    memset(buffer, 0, sizeof(buffer));
    fmt_float(buffer, tof, 4);
    printf("\"tof\": %s, ", buffer);
    memset(buffer, 0, sizeof(buffer));
    fmt_float(buffer, rssi, 4);
    printf("\"rssi\": %s, ", buffer);
    memset(buffer, 0, sizeof(buffer));
    fmt_float(buffer, los, 4);
    printf("\"los\": %s", buffer);
    printf("}\n");

    /* on completion of a range request setup an event to keep listening,
       if is anchor */
    dpl_eventq_put(dpl_eventq_dflt_get(), &_rng_req_complete_event);

    return true;
}

/**
 * @brief API for receive timeout callback.
 *
 * @param[in] inst  Pointer to struct uwb_dev.
 * @param[in] cbs   Pointer to struct uwb_mac_interface.
 *
 * @return true on success
 */
static bool _rx_timeout_cb(struct uwb_dev *inst, struct uwb_mac_interface *cbs)
{
    struct uwb_rng_instance *rng = (struct uwb_rng_instance *)cbs->inst_ptr;

    if (inst->role & UWB_ROLE_ANCHOR) {
        /* Restart receiver */
        uwb_phy_forcetrxoff(inst);
        uwb_rng_listen(rng, 0xfffff, UWB_NONBLOCKING);
    }
    return true;
}

/* Structure of extension callbacks common for mac layer */
static struct uwb_mac_interface _uwb_mac_cbs = (struct uwb_mac_interface){
    .id = UWBEXT_APP0,
    .complete_cb = _complete_cb,
    .rx_timeout_cb = _rx_timeout_cb,
};

/**
 * @brief In the example this function represents the event context
 * processing of the received range request which has been offloaded from
 * ISR context to an event queue.
 */
static void _rng_req_complete_cb(struct dpl_event *ev)
{
    assert(ev != NULL);

    struct uwb_rng_instance *rng = (struct uwb_rng_instance *)
        dpl_event_get_arg(ev);
    struct uwb_dev *inst = rng->dev_inst;

    if (inst->role & UWB_ROLE_ANCHOR) {
        uwb_rng_listen(rng, 0xfffff, UWB_NONBLOCKING);
    }
}

/**
 * @brief An event callback to send range request every _rng_activity->period.
 * On every request it will switch the used ranging algorithm.
 */
static void _uwb_ev_cb(struct dpl_event *ev)
{
    struct uwb_rng_instance *rng = (struct uwb_rng_instance *)ev->arg;
    struct uwb_dev *inst = rng->dev_inst;

    if (inst->role & UWB_ROLE_ANCHOR) {
        if (dpl_sem_get_count(&rng->sem) == 1) {
            uwb_rng_listen(rng, 0xfffff, UWB_NONBLOCKING);
        }
    }
    else {
        if (_rng_activity->algo != TWR_ALGORITHM_NONE) {
            uwb_rng_request(rng, _rng_activity->dest.u16,
                           (uwb_dataframe_code_t)_rng_activity->algo);
        }
    }

    /* reset the callback if ranging is enabled */
    if (_rng_activity->run) {
        dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);
    }
}

/**
 * @brief Simple callback to disable ranging activity after timeout
 */
static void _rng_activity_timeout(void* arg)
{
    (void) arg;
    _rng_activity->run = false;
}

void twr_rng_activity_init(twr_rng_activity_t* activity)
{
    /* Recover the instance pointer, only one is currently supported */
    struct uwb_dev *udev = uwb_dev_idx_lookup(0);
    struct uwb_rng_instance *rng =
        (struct uwb_rng_instance *)uwb_mac_find_cb_inst_ptr(udev, UWBEXT_RNG);
    assert(rng);

    /* Set range activity pointer */
    _rng_activity = activity;

    /* set up local mac callbacks */
    _uwb_mac_cbs.inst_ptr = rng;
    uwb_mac_append_interface(udev, &_uwb_mac_cbs);

    if (IS_USED(MODULE_UWB_CORE_TWR_SS_ACK)) {
        uwb_set_autoack(udev, true);
        uwb_set_autoack_delay(udev, 12);
    }

    /* Apply config */
    uwb_mac_config(udev, NULL);
    uwb_txrf_config(udev, &udev->config.txrf);

    /* Callout init */
    dpl_callout_init(&_rng_activity->callout, dpl_eventq_dflt_get(),
                     _uwb_ev_cb, rng);
    dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);

    /* Set up timeout uf needed */
    _rng_activity->timer.callback = _rng_activity_timeout;
    if (_rng_activity->timeout != DPL_TIMEOUT_NEVER) {
        xtimer_set64(&_rng_activity->timer, _rng_activity->timeout * US_PER_MS);
    }

    /* Setup offloading of completed range request */
    dpl_event_init(&_rng_req_complete_event, _rng_req_complete_cb, rng);

    /* Bootstrap the anchor */
    if ((udev->role & UWB_ROLE_ANCHOR)) {
        printf("Node role: ANCHOR \n");
        /* anchor starts listening by default */
        dpl_callout_reset(&_rng_activity->callout, _rng_activity->period);
        _rng_activity->run = true;
    }
}


