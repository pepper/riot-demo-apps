from exp_data import *
from rss_calibration import *
from plot_rss_calibration import rss_plot_pl_model_errors

from math import log10
import numpy as np
from scipy import stats

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tikzplotlib

from typing import Union, Type

from plot_commons import *


def plot_rss_results(exp_data: ExperimentData, pl_model:PathLossModel, ble=False):
    # get the data
    # it is assumed that the rssi list is externally mapped to exp_data.neighbors)
    rss = exp_data.ble_rss_list if ble else exp_data.uwb_rss_list 

    # 1- histogram of rss measurements
    fig = plt.figure()
    for i,node in enumerate(exp_data.neighbors):        
        plt.hist(rss[i], label=node.node_id, color=colors[i])
    plt.legend(ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)
    plt.title(f'{exp_data.initiator.network_address}')
    plt.xlabel(f'{"BLE" if ble else "UWB"} Rssi (dBm)')
    plt.ylabel('frequency')
    #plt.tight_layout()
    plt.show(block=False)

    # 2- Boxplot and stats of the raw rss measurements
    rss_stats_table = boxplot_stats(title_str=f'{exp_data.initiator.network_address}',  ylabel=f'{"BLE" if ble else "UWB"} Rssi (dBm)', xlabel='distance (m)',
                         values=rss, d=exp_data.distances,
                         node_ids=[n.node_id for n in exp_data.neighbors])

    # 3- Boxplot and stats of the range predictions
    rng_stats_table = rss_plot_pl_model_errors(exp_data, pl_model.ble_rss_model if ble else pl_model.uwb_rss_model, ble=ble,
                                    title_str= f'{"BLE" if ble else "UWB"} RSSI path loss model prediction')
    
    return rss_stats_table, rng_stats_table

def plot_twr_results(exp_data: ExperimentData):
    rng_list = exp_data.uwb_rng_list
    # 1- Boxplot and stats of the raw uwb range measurements
    rng_stats_table = boxplot_stats(title_str=f'{exp_data.initiator.network_address}',  ylabel='UWB Range', xlabel='distance (m)',
                         values=rng_list, d=exp_data.distances,
                         node_ids=[n.node_id for n in exp_data.neighbors])

    # 3- Boxplot and stats of the uwb range errors
    rng_errors = list()
    for i,di in enumerate(exp_data.distances):
        err = abs(np.array(exp_data.uwb_rng_list[i])-di)
        rng_errors.append(err)                                    
    rng_errors_stats_table =boxplot_stats(title_str=f'UWB range errors',  ylabel='Range error (m)', xlabel='distance (m)',
                         values=rng_errors, d=exp_data.distances,
                         node_ids=[n.node_id for n in exp_data.neighbors])

    return rng_stats_table, rng_errors_stats_table




def plot_ble_pathloss(exp_data: ExperimentData):
    # get sorted distances
    _, D = coords(exp_data)

    # Plots distributions of ble path loss for each neighbor node, put initiator node in title
    fig = plt.figure()
    for i,node in enumerate(exp_data.neighbors):
        path_loss = [m.tx_power-m.rss for m in exp_data.measurements[node.network_address].ble_rss_data]
        plt.hist(path_loss, label=node.node_id, color=colors[i])
    plt.legend(ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)

    plt.title(f'{exp_data.initiator.network_address}')
    plt.xlabel('BLE PathLoss (dBm)')
    plt.ylabel('frequency')
    plt.tight_layout()
    plt.show(block=False)

    # Boxplot path loss against distance from initiator
    path_loss_list = exp_data.ble_pathloss_list()
    fig, ax = plt.subplots()
    ax.set_title(f'{exp_data.initiator.network_address}')
    box = ax.boxplot(path_loss_list, showmeans=True, meanline=True, patch_artist=True)
    # fill with colors 
    for patch, color in zip(box['boxes'], colors):
        patch.set_facecolor(color)
    
    # show stats
    stats = dict()
    stats['mean'] = [np.mean(x) for x in path_loss_list]
    stats['std'] = [np.std(x) for x in path_loss_list]
    for i, line in enumerate(box['medians']):
        x, y = line.get_xydata()[1]
        text = f' μ={stats["mean"][i]:.2f}\n σ={stats["std"][i]:.2f}'
        ax.annotate(text, xy=(x, y+stats["std"][i]))
    # label samples with node ids
    #for i,node in enumerate(exp_data.neighbors):
    #    ax.annotate(exp_data.neighbors[i].node_id, xy=(i+1, .98), xycoords=("data", "axes fraction"), horizontalalignment='center', weight='bold')

    # distance on x axis
    plt.setp(ax, xticklabels=[f'{i:.3}' for i in D[1:,0]])
    #plt.setp(xtickNames, rotation=45, fontsize=8)
    plt.legend([*box["boxes"]], [n.node_id for n in exp_data.neighbors], ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)

    ax.yaxis.grid(True)
    plt.xlabel('distance (m)')
    plt.ylabel('BLE PathLoss (dBm)')

    # increase axis limits to avoid having annotations outside plot area
    (xmin,xmax) = ax.get_xlim()
    ax.set_xlim((xmin,xmax+.5))

def plot_uwb_rss(exp_data: ExperimentData):
    _, D = coords(exp_data)

    # Plots distributions of uwb path loss for each neighbor node, put initiator node in title
    fig = plt.figure()
    for i,node in enumerate(exp_data.neighbors):
        rss = [m.rss for m in exp_data.measurements[node.network_address].uwb_rss_data]
        plt.hist(rss, label=node.node_id, color=colors[i])
    plt.legend(ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)
    plt.title(f'{exp_data.initiator.network_address}')
    plt.xlabel('UWB Rssi (dBm)')
    plt.tight_layout()
    plt.show(block=False)

    # Boxplot rssi against distance from initiator
    rss_list = exp_data.uwb_rss_list()
    fig, ax = plt.subplots()
    ax.set_title(f'{exp_data.initiator.network_address}')
    box = ax.boxplot(rss_list, showmeans=True, meanline=True, patch_artist=True)
    # fill with colors 
    for patch, color in zip(box['boxes'], colors):
        patch.set_facecolor(color)
    # show stats
    stats = dict()
    stats['mean'] = [np.mean(x) for x in rss_list]
    stats['std'] = [np.std(x) for x in rss_list]
    for i, line in enumerate(box['medians']):
        x, y = line.get_xydata()[1]
        text = f' μ={stats["mean"][i]:.2f}\n σ={stats["std"][i]:.2f}'
        ax.annotate(text, xy=(x, y+stats["std"][i]))
    # label samples with node ids
    #for i,node in enumerate(exp_data.neighbors):
    #    ax.annotate(exp_data.neighbors[i].node_id, xy=(i+1, .98), xycoords=("data", "axes fraction"), horizontalalignment='center', weight='bold')

    # distance on x axis
    plt.setp(ax, xticklabels=[f'{i:.3}' for i in D[1:,0]])
    #plt.setp(xtickNames)#, rotation=45, fontsize=8)
    
    plt.legend([*box["boxes"]], [n.node_id for n in exp_data.neighbors], ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)
    
    ax.yaxis.grid(True)
    plt.xlabel('distance (m)')
    plt.ylabel('UWB RSSI (dBm)')
    
    # increase axis limits to avoid having annotations outside plot area
    (xmin,xmax) = ax.get_xlim()
    ax.set_xlim((xmin,xmax+.5))
    (ymin,ymax) = ax.get_ylim()
    ax.set_ylim((ymin,ymax+1))


def plot_uwb_dist(exp_data: ExperimentData):
    _, D = coords(exp_data)
    
    # Plots uwb range measurement for each neighbor node, put initiaor node in title
    fig = plt.figure()
    for i,node in enumerate(exp_data.neighbors):
        rng = exp_data.measurements[node.network_address].uwb_dist
        plt.hist(rng, label=node.node_id, color=colors[i])
    plt.legend(ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)
    plt.title(f'{exp_data.initiator.network_address}')
    plt.ylabel('frequency')
    plt.xlabel('UWB Range (m)')
    plt.tight_layout()
    plt.show(block=False)

    # Boxplot path loss against distance from initiator
    rng_list = exp_data.uwb_rng_list()
    fig, ax = plt.subplots()
    ax.set_title(f'{exp_data.initiator.network_address}')
    box = ax.boxplot(rng_list, showmeans=True, meanline=True, patch_artist=True, autorange=True)
    # fill with colors 
    for patch, color in zip(box['boxes'], colors):
        patch.set_facecolor(color)
    # show stats
    stats = dict()
    stats['mean'] = [np.mean(x) for x in rng_list]
    stats['std'] = [np.std(x) for x in rng_list]
    for i, line in enumerate(box['medians']):
        x, y = line.get_xydata()[1]
        text = f' μ={stats["mean"][i]:.2f}\n σ={stats["std"][i]:.2f}'
        ax.annotate(text, xy=(x, y+stats["std"][i]))
    # label samples with node ids
    #for i,node in enumerate(exp_data.neighbors):
    #    ax.annotate(exp_data.neighbors[i].node_id, xy=(i+1, .98), xycoords=("data", "axes fraction"), horizontalalignment='center', weight='bold')
    # distance on x axis
    plt.setp(ax, xticklabels=[f'{i:.3}' for i in D[1:,0]])
    #plt.setp(xtickNames)#, rotation=45, fontsize=8)

    plt.legend([*box["boxes"]], [n.node_id for n in exp_data.neighbors], ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -0.4))
    fig.subplots_adjust(bottom=0.25)
    
    ax.yaxis.grid(True)
    plt.xlabel('distance (m)')
    plt.ylabel('UWB range (m)')

    # increase axis limits to avoid having annotations outside plot area
    (xmin,xmax) = ax.get_xlim()
    ax.set_xlim((xmin,xmax+.5))
    (ymin,ymax) = ax.get_ylim()
    ax.set_ylim((ymin,ymax+.5))


# saves all pics an distance matrix
def save_all_pics(output_folder = './plots', tables=Dict[str,str]):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    for i in plt.get_fignums():
        plt.figure(i)
        plt.savefig(f'{output_folder}/figure{i}.png')
        #plt.savefig(f'{output_folder}/figure{i}.pgf', backend='pgf')
        tikzplotlib.save(f'{output_folder}/figure{i}.tikz')
    # dump distance table (euclidean distance matrix)
    latex_distance_table = get_distances_table(exp_data)
    print(latex_distance_table)
    for table_name,table in tables.items():
        print(table,  file=open(f'{output_folder}/{table_name}.tex', 'w'))
    

def parse_json_file(file_path: str, cls: Union[Type[ExperimentData], Type[PathLossModel]]) -> Union[ExperimentData, PathLossModel]:
    with open(file_path) as file:
        json_str = ''.join(file.readlines())
        return cls.from_json_str(json_str)


if __name__ == "__main__":
    # rapid uint test
    # load capture

    import sys
    assert len(sys.argv) == 3, f'Invalid syntax, usage: {sys.argv[0]} <path_to_capture_file>.json <path_to_rss_pl_file>.json'

    cap_file = sys.argv[1]
    model_file = sys.argv[2]

    print(f'plotting data from capture in {cap_file}')
    print(f'Using rss path loss model in {model_file}')
    # Parse experiment data and sort in increasing distance w.r.t initiator
    exp_data = parse_json_file(file_path=cap_file, cls=ExperimentData).clone(sort=True)
    # Parse rss ranging model for ble and uwb
    pl_model = parse_json_file(file_path=model_file, cls=PathLossModel)

    # plot Nodes spatial information
    plot_nodes(exp_data)
    plot_distance_matrix(exp_data)

    # prepare latex tables output
    latex_tables = dict()
   
    # plot BLE results: 
    #  - rssi distributions (histos+boxplot) - 
    #  - rssi range prediction performances
    latex_tables['ble_rss_stats_table'], latex_tables['ble_rss_rng_err_table'] = plot_rss_results(exp_data, pl_model,ble=True)


    # plot UWB results: 
    #  - rssi distributions (histos+boxplot)
    #  - rssi range prediction performances
    latex_tables['uwb_rss_stats_table'], latex_tables['uwb_rss_rng_err_table'] = plot_rss_results(exp_data, pl_model, ble=False)
    #  - TWR measurements distribution
    #  - TWR range prediction performances
    latex_tables['uwb_twr_rng_stats_table'], latex_tables['uwb_twr_rng_err_table'] = plot_twr_results(exp_data)
    
    # show plots and save output
    plt.show(block=False)
    save_all_pics(tables=latex_tables)

    input('Press enter to exit')        
