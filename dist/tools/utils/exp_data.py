import os
import json
from dataclasses import dataclass, asdict, field
from collections.abc import Iterable
from typing import List, Dict, Tuple, Union, Optional
from dacite import from_dict

import numpy as np

@dataclass
class ExperimentNode:
    network_address: str                              # iot-lab format, example: dwm1001-5.saclay.iot-lab.info
    position: Union[List[float], Tuple[float, ...]]   # (x, y, z)
     
    @property
    def node_id(self) -> str:
        # iot-lab node id, example: dwm1001-5
        return self.network_address.split('.')[0]

@dataclass
class RssMeasurement:
    tx_power : Optional[float]
    rss : float

@dataclass
class MeasurementData:
    ble_address: str           # Mac address (64-bits)
    ble_rss_data: List[RssMeasurement]
    uwb_address: str           # Short address (16-bits)
    uwb_rss_data: List[RssMeasurement]
    uwb_dist: List[float]      # uwb range estimate in meters


@dataclass
class ExperimentData:
    initiator: ExperimentNode
    neighbors: List[ExperimentNode]
    # key: node_id in iot-lab notation
    measurements: Dict[str, MeasurementData]

    @property
    def distances(self) -> List[float]:
        # returns the list of distances between the neighbors and in the intiator
        return [np.linalg.norm(np.array(nei.position) - np.array(self.initiator.position)) for nei in self.neighbors]

    @property
    def ble_rss_list(self) -> List[List[float]]:
        # helper to extract a view of of BLE rss measurements as a list of lists in the form rss = [[rss_neighbor_1], [rss_neighbor_2] ... [rss_neighbor_n]]
        rss = lambda data: [m.rss for m in data]
        return [rss(self.measurements[node.network_address].ble_rss_data) for node in self.neighbors]

    @property
    def ble_pathloss_list(self) -> List[List[float]]:
        #FIXME remove this method, since the computed pathloss is not used and may introduce confusion
        # helper to extract a view of of pathloss measurements as a list of lists in the form PL = [[pl_neighbor_1], [pl_neighbor_2] ... [pl_neighbor_2]]
        import warnings
        warnings.warn("Calling a deprecated method ble_pathloss_list")
        pathloss = lambda rss_data: [*map(lambda m: m.tx_power-m.rss, rss_data)]
        return [pathloss(self.measurements[node.network_address].ble_rss_data) for node in self.neighbors]

    @property
    def uwb_rss_list(self) -> List[List[float]]:
         # helper to extract a view of of UWB rss measurements as a list of lists in the form rss = [[rss_neighbor_1], [rss_neighbor_2] ... [rss_neighbor_n]]
        rss = lambda data: [m.rss for m in data]
        return [rss(self.measurements[node.network_address].uwb_rss_data) for node in self.neighbors]

    @property
    def uwb_rng_list(self) -> List[List[float]]:
        # helper to extract a view of of UWB distance measurements as a list of lists in the form rng = [[rng_neighbor_1], [rng_neighbor_2] ... [rng_neighbor_n]]
        return [self.measurements[node.network_address].uwb_dist for node in self.neighbors]

    def clone(self, sort=False):
        if not sort:
            cp = from_dict(data_class=ExperimentData, data=asdict(self))
        else:
            # sort measurements with increasing distance from initiator
            sort_func = lambda node: np.linalg.norm(np.array(node.position)-np.array(self.initiator.position))
            _neighbors = sorted(self.neighbors, key=sort_func)
            _id_neighbors = [n.network_address for n in _neighbors]
            _meas_neighbors = [self.measurements[addr] for addr in _id_neighbors]
            cp = ExperimentData(initiator=self.initiator,
                                neighbors=_neighbors,
                                measurements=dict(zip(_id_neighbors, _meas_neighbors))
                                )
        return cp
        

    def to_json_str(self, indent=None) -> str:
        json_dict = asdict(self)
        return json.dumps(json_dict, indent=indent)

    @staticmethod
    def from_json_str(json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=ExperimentData, data=json_dict)


@dataclass
class Node:
    addr: List[str]
    addr_type: int
    name: str
    adv_type: int
    ad_data: List[str]
    last_rssi: int
    txpwr_dbm: int
    adv_msg_cnt: int
    first_update: int
    last_update: int

    def mac_address(self) -> str:
        return ''.join(self.addr)

    def adv_int(self) -> int:
        return (self.last_update - self.first_update) / self.adv_msg_cnt


@dataclass
class ScanData:
    nodes: List[Node]

    def to_dict(self) -> Dict[str, Node]:
        mac_addr = map(lambda node: node.mac_address(), self.nodes)
        return dict(zip(mac_addr, self.nodes))

    def rss_data(self) -> Dict[str, int]:
        mac_addr = map(lambda node: node.mac_address(), self.nodes)
        rssi = map(lambda node: node.last_rssi, self.nodes)
        return dict(zip(mac_addr, rssi))

    def to_json_str(self) -> str:
        json_dict = asdict(self)
        return json.dumps(json_dict)

    @staticmethod
    def from_json_str(json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=ScanData, data=json_dict)


@dataclass
class TwrData:
    utime: int
    uid: int
    ouid: int
    raz: List[float]
    rssi: float
    tof: float
    los: float

    def euid(self) -> str:
        eui = self.uid
        return '{:0>4X}'.format(((eui << 8) | (eui >> 8)) & 0xFFFF)

    def dest_euid(self) -> str:
        eui = self.ouid
        return '{:0>4X}'.format(((eui << 8) | (eui >> 8)) & 0xFFFF)

    @staticmethod
    def from_json_str(json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=TwrData, data=json_dict)
