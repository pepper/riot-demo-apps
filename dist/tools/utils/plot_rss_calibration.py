import sys
from matplotlib import pyplot as plt
import tikzplotlib

import numpy as np

from exp_data import ExperimentData
from rss_calibration import PathLossModel, PleEstimator, PleLeastSquareEstimator

from plot_commons import colors, rss_plot_pl_model_errors

def rss_plot_pl_model_fit(exp_data:ExperimentData, pl_model:PleEstimator, title_str:str):
    # Plotting helpers
    def get_data(pl_model):
        x = list()
        y = list()
        for i,di in enumerate(pl_model.d):
            for rssi in pl_model.rssi[i]:
                x.append(di)
                y.append(rssi)
        return x,y
    
    def eval_model(pl_model:PleEstimator):
        x = list()
        y = list()        
        
        for di in pl_model.d:
            rssi = pl_model.eval_rss(di)
            x.append(di)
            y.append(rssi)
        return x,y


    # model fitness curve
    fig=plt.figure()
    fig.canvas.set_window_title(f'figure {fig.number}-{title_str}')
    plt.plot(*get_data(pl_model), marker='o',linestyle = 'None') # data
    plt.plot(*eval_model(pl_model), marker=None) # model
    ax_title = ''.join(pl_model.print_path_loss_model(stdout=False, show_distance=False))
    if isinstance(pl_model, PleLeastSquareEstimator):
        ax_title = f'{ax_title}\nR^2 = {pl_model.linear_model.r_squared:.3}'

    plt.title(ax_title, fontsize='small')
    plt.xlabel('d (m)')
    plt.ylabel('Rssi (dBm)')
    plt.grid(True)
    plt.gca().set_xscale('log')
    plt.legend(['data', f'{pl_model.name()} fit'])
    plt.show(block=False)


def save_all_pics(output_folder):
    import os
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    for i in plt.get_fignums():
        fig = plt.figure(i)
        window_title = fig.canvas.get_window_title().replace(' ','_')
        output_file = f'{output_folder}/{window_title}'
        plt.savefig(f'{output_file}.png')
        #plt.savefig(f'{output_file}.pgf', backend='pgf')
        #tikzplotlib.clean_figure()
        tikzplotlib.save(f'{output_file}.tikz')
        
        


if __name__ == "__main__":
    # rapid unit test
    cap_file = sys.argv[1]
    print(f'plotting data from capture in {cap_file}')
    with open(cap_file) as f_log:
        json_str = ''.join(f_log.readlines())
        pl_model = PathLossModel(exp_data = ExperimentData.from_json_str(json_str))
        #pl_model = PathLossModel.from_json_str(json_str) # Test deserialization by passing the model json instead of experiment data json

        # plot model fit curve and modeling errors
        rss_plot_pl_model_fit(pl_model.exp_data, pl_model.ble_rss_model, 
                              title_str='BLE pathloss model')
        ble_range_errors = rss_plot_pl_model_errors(pl_model.exp_data, pl_model.ble_rss_model, ble=True,
                              title_str='BLE pathloss model')
        
        rss_plot_pl_model_fit(pl_model.exp_data, pl_model.uwb_rss_model,
                              title_str='UWB pathloss model')
        uwb_range_errors = rss_plot_pl_model_errors(pl_model.exp_data, pl_model.uwb_rss_model, ble=False,
                              title_str='UWB pathloss model')
        # display models infos
        print('>> BLE path-loss model:')
        pl_model.ble_rss_model.print_path_loss_model()
        print(f' R^2 = {pl_model.ble_rss_model.linear_model.r_squared}')
        print('>> UWB path-loss model:')
        pl_model.uwb_rss_model.print_path_loss_model()
        print(f' R^2 = {pl_model.uwb_rss_model.linear_model.r_squared}')

        # save output plots and model
        output_folder = './model-plots'
        save_all_pics(output_folder)
        print(ble_range_errors,  file=open(f'{output_folder}/ble_range_errors_table.tex', 'w'))
        print(uwb_range_errors,  file=open(f'{output_folder}/uwb_range_errors_table.tex', 'w'))

        print(pl_model.to_json_str(),  file=open(f'{output_folder}/rss_pl_model.json', 'w'))

        input('Press enter to exit')




###### Experimental code, TODO remove to more adequate area
'''
def rss_plot_ple_models(models:List[PleEstimator], title_str:str):
    # plots all available estimators for fittting rssi = f(d)
        # Plotting helpers
        def get_data(rssi_list, d):
            x = list()
            y = list()
            for i,di in enumerate(d):
                for rssi in rssi_list[i]:
                    x.append(di)
                    y.append(rssi)
            return x,y
        
        def get_model(rssi_list, d, pl_model:PleEstimator):
            x = list()
            y = list()        
            
            for di in d:
                rssi = pl_model.eval_rss(di)
                x.append(di)
                y.append(rssi)
            return x,y
        
        def get_prediction_error(rssi_list, d, pl_model:PleEstimator):
            def rmse(d0,di):
                return np.sqrt(np.mean((np.array(di)-d0)**2))

            y = list()
            for i,di in enumerate(d):
                dest = [pl_model.range_estimate(rssi) for rssi in rssi_list[i]]
                #print(f'get_prediction_error: d[{i}] = {di}, dest = {dest}, rmse = {rmse(di, dest)}')
                y.append(rmse(di, dest))
            return d,y
        
        def get_android_prediction_error(rssi_list, d, pl_model:PleEstimator):
            def rmse(d0,di):
                return np.sqrt(np.mean((np.array(di)-d0)**2))

            y = list()
            android_model = AndroidEstimator(txPower=pl_model.eval_rss(d=1.0))
            for i,di in enumerate(d):
                dest = [android_model.range_estimate(P_Rx=rssi) for rssi in rssi_list[i]]
                y.append(rmse(di, dest))

            return d,y

        # make sure models are based on the same data
        distances = [model.d for model in models]
        rssis = [model.rssi for model in models]
        d = distances[0]
        rssi = rssis[0]
        for di,rss in zip(distances, rssis):
            assert(d==di)
            assert(rss==rssi)
        
        # plot data and models
        plt.figure()
        plt.plot(*get_data(rssi, d), marker='o',linestyle = 'None') # data
        for model in models:
            plt.plot(*get_model(rssi, d, model), marker=None)
        plt.title(title_str)
        plt.xlabel('d (m)')
        plt.ylabel('Rssi (dBm)')
        plt.grid(True)
        plt.gca().set_xscale('log')
        plt.legend(['data', *[model.name() for model in models]])
        plt.show(block=False)

        # box plot model errors: predicted distance against distance
        plt.figure()
        for model in models:
            if model.name() == '10Max':
                print('>>>>>>>>>>>>>>> Gotcha')
                plt.scatter(*get_prediction_error(rssi, d, model),marker='+')
            else:
                plt.scatter(*get_prediction_error(rssi, d, model))
        
        plt.xlabel('d (m)')
        plt.ylabel('|d - d_est|')
        plt.grid(True)
        plt.legend([model.name() for model in models])
        plt.title(f'{title_str} errors')
        plt.show(block=False)

        plt.figure()
        for model in models:
            if isinstance(model, PleTenMaxEstimator):
                print('>>>>>>>>>>>>>>> Gotcha')
                plt.scatter(*get_android_prediction_error(rssi, d, model),marker='+')
            else:
                plt.scatter(*get_android_prediction_error(rssi, d, model))
        
        plt.xlabel('d (m)')
        plt.ylabel('|d - d_est|')
        plt.grid(True)
        plt.legend([model.name() for model in models])
        plt.title(f'{title_str} android model errors')
        plt.show(block=False)
        

def rss_check_models(model: PathLossModel):
    ble_pl_model = model.ble_rss_model
    uwb_pl_model = model.uwb_rss_model
    assert(ble_pl_model.d == uwb_pl_model.d)
    dist = ble_pl_model.d
    exp_data = model.exp_data
    

    def _check_model(rss_list: List[float], _model:PleEstimator):
        android_model = AndroidEstimator(txPower=_model.eval_rss(d=1.0))
        for i,rss in enumerate(rss_list):
            dest = list()
            android_dest = list()
            for j,sample in enumerate(rss):
                dest.append(_model.range_estimate(sample))
                android_dest.append(android_model.range_estimate(P_Rx=sample))
            print(f'>> d[{i}] = {dist[i]}, dest[{i}] = {dest}, android_dest[{i}] = {android_dest}')
            print(f'>> d[{i}] = {dist[i]}, mean_dest[{i}] = {np.mean(dest)}, median_dest[{i}] = {np.median(dest)}, android_mean_dest[{i}] = {np.mean(android_dest)}, android_median_dest[{i}] = {np.median(android_dest)}')
    
    ## BLE rss measurements v.s distance
    def _filter_android(rss:List[float]):
        rss = sorted(rss)
        size = len(rss)
        if (size>2):
            start = int(size/10)+1
            end = size-int(size/10)-2
            rss = rss[start:end]
        return np.mean(rss)    
    print(f'BLE path-loss model: {ble_pl_model}')
    ble_pl_model.print_path_loss_model()
    print(f'BLE model R^2={pl_model.ble_rss_model.linear_model.r_squared}')
    for i,r in enumerate(ble_pl_model.rssi):
        print(f'{ble_pl_model.d[i]}\t{_filter_android(r)}')
    #_check_model(exp_data.ble_rss_list(), ble_pl_model)


    # UWB rss measurements v.s distance
    uwb_pl_model = PleLeastSquareEstimator(rssi=exp_data.uwb_rss_list(), d=dist)
    print(f'UWB path-loss model: {uwb_pl_model}')
    uwb_pl_model.print_path_loss_model()
    print(f'UWB model R^2={pl_model.uwb_rss_model.linear_model.r_squared}')
    #_check_model(exp_data.uwb_rss_list(), uwb_pl_model)
    for i,r in enumerate(uwb_pl_model.rssi):
        print(f'{uwb_pl_model.d[i]}\t{_filter_android(r)}')
'''    
