from math import log10
import numpy as np
from scipy import stats

from dataclasses import dataclass, field, asdict
from typing import List, Optional, Dict
from abc import abstractmethod

import json
from dacite import from_dict

from exp_data import ExperimentData

@dataclass
class PleEstimator:
    # input vars: dictionary of measurement rssi[i] = rssi measurements at distance d[i]
    #measurements : List[RssiMeasurements]
    rssi:List[List[float]]
    d : List[float]

    # computed model vars from input ones
    ple: float = field(init=False)
    d0: float = field(init=False)
    rss0: [float,float] = field(init=False)

    def __post_init__(self):
        self.ple, self.d0, self.rss0 = self._ple_estimate()
        
    @abstractmethod
    def _ple_estimate(self) -> (float,float,float):
        # Subclasses must implement the internal algorithm for ple estimation. Returns an ordered list:
        # (estimated pathlos, reference distance d0, rssi at reference distance d0)
        pass

    @abstractmethod
    def name(self) -> str:
        return 'abstract model'

    def print_path_loss_model(self, stdout=True, show_distance=True):
        output = [f'Rssi(d) = {self.rss0:.4} - 10x{self.ple:.4}xlog10(d/{self.d0:.4})) dBm']

        if show_distance:
            output.append(f'\nd = {self.d0:.4}x10^(({self.rss0:.4}-Rssi(d))/(10x{self.ple:.4})) m')

        if stdout:
            print(*output)
            
        return output

    def range_estimate(self, rssi_sample: float) -> float:
        # returns the range estimate from the calibrated rss model
        dest = self.d0*(10.0**((self.rss0-rssi_sample)/(10.0*self.ple)))
        return dest

    def eval_rss(self, d:float) -> float:
        # returns the rssi in dBm value at a given distance in meters
        return self.rss0 - 10.0*self.ple*log10(d/self.d0)

    def to_json_str(self, indent=None) -> str:
        json_dict = asdict(self)
        return json.dumps(json_dict, indent=indent)
    
    @classmethod
    def from_json_str(cls, json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=cls, data=json_dict)


@dataclass 
class LinearRegression:
    # input data
    x : List[float]
    y : List[float]

    # Linear Regression output of scipy.stats.linregress
    slope : float = field(init=False)
    intercept : float = field(init=False)
    r_value : float = field(init=False)
    p_value : float = field(init=False)
    std_err : float = field(init=False)

    def __post_init__(self):
        # fit linear regression
        self.slope, self.intercept, self.r_value, self.p_value, self.std_err = stats.linregress(self.x, self.y)
    
    @property
    def r_squared(self):
        # model fitness indicator: should be higher than 0.8 for a good fit, if less than 0.5 the linear model does not fit 
        return (self.r_value)**2

@dataclass
class PleLeastSquareEstimator(PleEstimator):
    # an averaging estimator of the Path-Loss Exponent (PLE), it takes the first distance as reference point
    linear_model : LinearRegression = field(init=False)

    def name(self):
        return 'Least-Squares'
    
    def _ple_estimate(self) ->  (float,float,float):
        d0 = 1.0 # set reference distance to d0=1m
        # the model becomes Rssi(d) = Rssi(1m) - 1O*PLE*log10(d)
        # since the data consists od Rssi(d) with d known, the uknowns Rssi(1m) and PLE can found using linear regression

        # build x=log10(d), y = Rssi(d)
        x = list()
        y = list()
        for i,di in enumerate(self.d):
            for rss in self.rssi[i]:
                x.append(log10(di))
                y.append(rss)

        # fit linear regression
        self.linear_model = LinearRegression(x, y)

        # get pathloss
        avg_ple = -self.linear_model.slope/10.0
        rss0 = self.linear_model.intercept  # log10(d) = 0 when d0=1m

        return avg_ple, d0, rss0


@dataclass
class PathLossModel:
    # wraps experiment data, sorts its measurements by distance w.r.t initiator node, 
    # and computes least-square Path loss models for BLE and UWB rssi measurements
    exp_data : ExperimentData   # captured data

    # Least-square path loss estimators 
    ble_rss_model: PleLeastSquareEstimator = field(init=False)  #  Path Loss model based on BLE Rssi captured data
    uwb_rss_model: PleLeastSquareEstimator = field(init=False)  #  Path Loss model based on UWB Rssi captured data
    
    def __post_init__(self):
        self.exp_data = self.exp_data.clone(sort=True)  # ensure measurements are sorted by increasing distance w.r.t initiator node
        dist = self.exp_data.distances
        # BLE least-square model
        self.ble_rss_model = PleLeastSquareEstimator(rssi=self.exp_data.ble_rss_list, d=dist)
        #self.ble_rss_model = PleLeastSquareEstimator(RssiMeasurements.from_lists(distances=dist, rssi_d=self.exp_data.ble_rss_list)
        # UWB least-square model
        self.uwb_rss_model = PleLeastSquareEstimator(rssi=self.exp_data.uwb_rss_list, d=dist)
        #self.uwb_rss_model = PleLeastSquareEstimator(RssiMeasurements.from_lists(distances=dist, rssi_d=self.exp_data.uwb_rss_list)

        
    def predict_distance(self, rssi:float, ble=False) -> float:
        # shortcut to predict range according to given rssi based on the BLE or UWB path loss model
        model = self.ble_rss_model if ble else self.uwb_rss_model
        return model.range_estimate(rssi)
    
    def to_json_str(self, indent=None) -> str:
        json_dict = asdict(self)
        return json.dumps(json_dict, indent=indent)

    @classmethod
    def from_json_str(cls, json_string: str):
        json_dict = json.loads(json_string)
        # fails via dacite, do it manually
        #return from_dict(data_class=cls, data=json_dict)
        exp_data = ExperimentData.from_json_str(json.dumps(json_dict['exp_data']))
        return PathLossModel(exp_data)

### Experimental alternative path loss model estimators
@dataclass
class PleAvgEstimator(PleEstimator):
    use_median: Optional[bool] = False
    
    def name(self):
        return 'Median' if self.use_median else 'Mean'

    # an averaging estimator of the Path-Loss Exponent (PLE), it takes the first distance as reference point
    def _ple_estimate(self) ->  (float,float,float):

        # averaging helper
        def _avg(values:List):
            return np.median(np.array(values)) if self.use_median else np.mean(np.array(values))

        # pathlos formula helper
        def _ple_formula(rss0: float, d0: float, rss: float, di=float) -> float:
            return ((rss0-rss)/(10.0*log10(di/d0)))

        # average the measurements
        rssi = [_avg(m) for m in self.rssi]

        # do modeling
        ## Select the reference point set to the first node, TODO pass as parameter
        ref_index = 0
        d0 = self.d[ref_index]
        rss0 = rssi[ref_index]
    
        # estimate path los exponent (PLE)
        ##drop reference point from measurements
        rssi.pop(ref_index)
        d = self.d.copy()
        d.pop(ref_index)
        # apply the formula
        ple_est = [_ple_formula(rss0, d0, ri, di) for ri,di in zip(rssi,d)]
        avg_ple = _avg(ple_est)

        return avg_ple, d0, rss0

@dataclass
class PleMinMaxEstimator(PleEstimator):
    use_min: Optional[bool] = False
    
    def name(self):
        return 'Min' if self.use_min else 'Max'

    # an averaging estimator of the Path-Loss Exponent (PLE), it takes the first distance as reference point
    def _ple_estimate(self) ->  (float,float,float):

        # minmax helper
        def _minmax(values:List):
            return np.min(np.array(values)) if self.use_min else np.max(np.array(values))

        # pathlos formula helper
        def _ple_formula(rss0: float, d0: float, rss: float, di=float) -> float:
            return ((rss0-rss)/(10.0*log10(di/d0)))

        # average the measurements
        rssi = [_minmax(m) for m in self.rssi]

        # do modeling
        ## Select the reference point set to the first node, TODO pass as parameter
        ref_index = 0
        d0 = self.d[ref_index]
        rss0 = rssi[ref_index]
    
        # estimate path los exponent (PLE)
        ##drop reference point from measurements
        rssi.pop(ref_index)
        d = self.d.copy()
        d.pop(ref_index)
        # apply the formula
        ple_est = [_ple_formula(rss0, d0, ri, di) for ri,di in zip(rssi,d)]
        avg_ple = _minmax(ple_est)

        return avg_ple, d0, rss0

@dataclass
class PleTenMaxEstimator(PleEstimator):
    
    def name(self):
        return '10Max'

    # an averaging estimator of the Path-Loss Exponent (PLE), it takes the first distance as reference point
    def _ple_estimate(self) ->  (float,float,float):

        # minmax helper
        def _filter(values:List):
            values = sorted(values,reverse=True)
            assert(len(values)>=10)
            return np.mean(values[0:10])

        # pathlos formula helper
        def _ple_formula(rss0: float, d0: float, rss: float, di=float) -> float:
            return ((rss0-rss)/(10.0*log10(di/d0)))

        # average the measurements by taking the 10 highest samples
        rssi = [_filter(m) for m in self.rssi]

        # do modeling
        ## Select the reference point set to the first node, TODO pass as parameter
        ref_index = 0
        d0 = self.d[ref_index]
        rss0 = np.mean(rssi[ref_index])
    
        # estimate path los exponent (PLE)
        ##drop reference point from measurements
        rssi.pop(ref_index)
        d = self.d.copy()
        d.pop(ref_index)
        # apply the formula
        ple_est = [_ple_formula(rss0, d0, ri, di) for ri,di in zip(rssi,d)]
        avg_ple = np.mean(ple_est)

        return avg_ple, d0, rss0
    
     
@dataclass
class AndroidEstimator:
    txPower : float # received power at the reference distance of 1 m (i.e reference power)
    
    def range_estimate(self, P_Rx:float) -> float:
        # returns a range estimate according the android beacon library model for indoors.
        # input 'P_Rx': received power at a fixed distance
        #
        # see discussion on : https://stackoverflow.com/questions/21338031/radius-networks-ibeacon-ranging-fluctuation/21340147#21340147
        # see: https://developer.radiusnetworks.com/2014/12/04/fundamentals-of-beacon-ranging.html
        # see also paper on H.-s. Choet al., “Accurate distance estimation between things: A self-correcting  approach,”Open  Journal  of  Internet  Of  Things  (OJIOT),vol. 1, no. 2, pp. 19–27, 2015.: 
        #
        # Important Note: 
        #  The android beacon library finally provides a mechanism for calibrating a pathloss model depending on the smartphone manufacturer model
        #  (coeff1)*((P_Rx/self.txPower)**(coeff2)) + coeff3
        #  The list of available path loss models is a available here: https://github.com/AltBeacon/android-beacon-library/blob/master/lib/src/main/resources/model-distance-calculations.json
        #  Details on calibrating the path loss model for a phone is given here: https://altbeacon.github.io/android-beacon-library/distance-calculations2.html
        #  This requires to have a mean estimate of reception power at 1m, then perform a set of mean rssi over a set of increasing distances (including the reference 1m distance)
        
        return (0.89976)*((P_Rx/self.txPower)**(7.7095)) + 0.111   # estimated distance



# Factory method
def available_path_loss_models(rssi_list, dist) -> List[PleEstimator]:
    models = [PleLeastSquareEstimator(rssi=rssi_list, d=dist),
              #PleAvgEstimator(rssi=rssi_list, d=dist,use_median=False),
              #PleAvgEstimator(rssi=rssi_list, d=dist,use_median=True),
              #PleMinMaxEstimator(rssi=rssi_list, d=dist, use_min=False),
              #PleTenMaxEstimator(rssi=rssi_list, d=dist)
            ]
    names = [model.name() for model in models]
    return dict(zip(names,models))


if __name__ == "__main__":
    d = [i+1 for i in range(3)]
    rssi = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    meas = list()
    for i,di in enumerate(d):
        meas.append(RssiMeasurements(rssi=rssi[i],d=di))
    print(meas)
    ple = PleLeastSquareEstimator(measurements=meas)
    print(ple)
    json_str = ple.to_json_str()
    print(json_str)
    print(PleLeastSquareEstimator.from_json_str(json_str))
