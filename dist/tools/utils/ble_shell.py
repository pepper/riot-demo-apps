# Copyright (C) 2020 Inria
#
# This file is subject to the terms and conditions of the GNU Lesser
# General Public License v2.1. See the file LICENSE in the top level
# directory for more details.

"""
twr-application shell interactions

Defines twr-activity related shell command interactions
"""
import re

from riotctrl.shell import ShellInteraction, ShellInteractionParser


class BleScanTxPowerParser(ShellInteractionParser):
    reg = r'Current Tx Power (\d+) dBm'


    def parse(self, cmd_output):
        tx_power = None
        match = re.search(reg, cmd_output)
        if match is not None:
            tx_power = match.group(1)
        return tx_power


class BleScan(ShellInteraction):
    @ShellInteraction.check_term
    def scan(self, scan_time, scan_cycles, out_type='json',
             timeout=-1, async_=False):
        cmd = "scan {} {} {}".format(out_type, int(scan_time), int(scan_cycles))
        return self.cmd(cmd, timeout, async_)


    def autoadv_cmd(self, args=None, timeout=-1, async_=False):
        cmd = "autoadv"
        if args is not None:
            cmd += " {args}".format(args=" ".join(str(a) for a in args))
        return self.cmd(cmd, timeout=timeout, async_=False)


    def autoadv_start(self, time='', timeout=-1, async_=False):
        return self.autoadv_cmd(args=("start", time),
                            timeout=timeout, async_=async_)


    def autoadv_stop(self, timeout=-1, async_=False):
        return self.autoadv_cmd(args=("stop",),
                            timeout=timeout, async_=async_)


    def autoadv_pwr(self, timeout=-1, async_=False):
        return self.autoadv_cmd(args=("pwr",),
                            timeout=timeout, async_=async_)


    def autoadv_name(self, name='', timeout=-1, async_=False):
        return self.autoadv_cmd(args=("name", name),
                            timeout=timeout, async_=async_)
