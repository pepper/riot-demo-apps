import numpy as np

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import tikzplotlib

from tabulate import tabulate


from exp_data import ExperimentData, ExperimentNode
from rss_calibration import PleEstimator

from typing import List

# distinct colors used to plot
colors = ['blue', 'green', 'purple', 'tan', 'pink', 'red', 'cyan', 'magenta', 'gray', 'black']


def plot_nodes(exp_data: ExperimentData):
    # plots nodes according to their positions and with labels
    # include distance in between or w.r.t to first node

    fig = plt.figure()
    fig.suptitle("Spatial distribution of the nodes")
    ax = Axes3D(fig)

    def plot_node(node: ExperimentNode,  marker='o', marker_color='k', marker_size=24,
                  label_color='k', label_size=12):
        (x, y, z) = node.position
        ax.scatter(x, y, z, c=marker_color, marker=marker, s=marker_size)
        ax.text(x, y, z, node.network_address.split('.')[0],
                size=label_size, color=label_color)

    # plot initiator
    plot_node(exp_data.initiator, marker='^', marker_color='r')
    for node in exp_data.neighbors:
        plot_node(node)
    ax.set_xlabel('x(m)')
    ax.set_ylabel('y(m)')
    ax.set_zlabel('z(m)')
    plt.show(block=False)


def plot_distance_matrix(exp_data: ExperimentData):
    # get inter-node distances
    _, D = coords(exp_data)
    n = D.shape[0]

    _, ax = plt.subplots()
    ax.imshow(D)
    labels = [exp_data.initiator.node_id, *(n.node_id for n in exp_data.neighbors)]
    ax.set_xticks(range(n))
    ax.set_yticks(range(n))
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(n):
        for j in range(n):
            ax.text(j, i, f'{D[i, j]:.4}', ha="center", va="center", color="w")

    ax.set_title("Euclidean distance between nodes (in m)")
    plt.show(block=False)


# dump distance data as latex table ;)
def get_distances_table(exp_data: ExperimentData) -> str:
    exp_data = exp_data.clone(sort=True) # sort distance w.r.t to initiator
    _, D = coords(exp_data)
    node_ids = [int(n.node_id.split('-')[1]) for n in [exp_data.initiator, *exp_data.neighbors]]
    tbl = [['id', *node_ids]]
    for i,node_id in enumerate(node_ids):
        _lst = [node_id, *D[i,:]]
        tbl.append(_lst)
    latex_tbl = tabulate(tbl, tablefmt="latex", floatfmt=".2f", headers="firstrow")
    return str(latex_tbl)


# Model Prediction Errors plot. Returns a string containg a latex table representation of model errors 
def rss_plot_pl_model_errors(exp_data:ExperimentData, pl_model:PleEstimator, title_str:str, ble:bool) -> str:
    def eval_model_errors():
        #get rssi experimental data
        rssi_list = exp_data.ble_rss_list if ble else exp_data.uwb_rss_list 
        y = list()
        for i,di in enumerate(exp_data.distances):
            #dest = [pl_model.range_estimate(rssi) for rssi in pl_model.rssi[i]]
            dest = [pl_model.range_estimate(rssi) for rssi in rssi_list[i]]
            #rmse = np.sqrt(np.mean((np.array(di)-dest)**2))
            #print(f'get_prediction_error: d[{i}] = {di}, dest = {dest}, rmse = {rmse}')
            y.append(abs(np.array(dest)-di))
        return y
    
    model_errors = eval_model_errors()
    return boxplot_stats(title_str=f'{title_str} errors',  ylabel='Range error (m)', xlabel='distance (m)',
                         values=model_errors, d=exp_data.distances,
                         node_ids=[n.node_id for n in exp_data.neighbors])


def boxplot_stats(title_str:str,  ylabel:str, xlabel:str,
                 values:List[List[float]], node_ids :List[float], d:List[float]) -> str:
    assert len(values) == len(node_ids)== len(node_ids) , 'Invalid data parameters, must have same length'
    assert len(values) <= len(colors), 'Not enough colors to discrilminate distances (nodes), add more colors'

    #fig, axs = plt.subplots(2,1)
    #plt.subplots_adjust(hspace=1.5)

    ## model erros boxplot
    #ax = axs[0]
    fig, ax = plt.subplots()

    #ax.set_title(f'{title_str}')
    fig.canvas.set_window_title(f'figure {fig.number}-boxplot-{title_str}')
    box = ax.boxplot(values, showmeans=True, meanline=True, patch_artist=True, flierprops=dict(marker='o', markersize=1))
    # fill with colors 
    for patch, color in zip(box['boxes'], colors):
        patch.set_facecolor(color)
    
    # distance on x axis
    plt.setp(ax, xticklabels=[f'{di:.3}' for di in d])
    #plt.setp(xtickNames, rotation=45, fontsize=8)
    #ax.legend([*box["boxes"]], node_ids, ncol=1, fancybox=True, shadow=True, loc="upper left", bbox_to_anchor=(1,1))
    #fig.subplots_adjust(right=0.8)
    ax.legend([*box["boxes"]], node_ids, ncol=3, fancybox=True, shadow=True, loc="lower left", bbox_to_anchor=(0, -.45))
    ax.axis('tight')
    ax.yaxis.grid(True)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.subplots_adjust(bottom=0.3)

    ## show stats table in sublot and return a latex tring representation
    #ax = axs[1]
    fig, ax = plt.subplots()
    latex_table = plot_stats_table(ax, values, node_ids, d)
    fig.canvas.set_window_title(f'figure {fig.number}-table-{title_str}')


    plt.show(block=False)
    return latex_table



# Plots statistics on given axis
def plot_stats_table(ax, values:List[List[float]], node_ids :List[float], d:List[float]) -> str:
    assert len(values) == len(node_ids)== len(node_ids) , 'Inconsistent data parameters, must have same length'
    # stats
    stats = dict()
    stats['mean'] = [np.mean(x) for x in values]
    stats['median'] = [np.median(x)for x in values]
    stats['std'] = [np.std(x) for x in values]
    # Prepare table
    tbl_cols = ['node id', 'distance', 'mean', 'median', 'st. deviation']
    tbl_vals = list()
    for i,di in enumerate(d):
        row = [node_ids[i], f'{di:.3}', f'{stats["mean"][i]:.3}', f'{stats["median"][i]:.3}', f'{stats["std"][i]:.3}']
        tbl_vals.append(row)
    # plot it
    ax.axis('off')
    ax.axis('tight')
    ax.table(cellText=tbl_vals, colLabels=tbl_cols, loc='center')
    
    # export it to latex
    latex_table = [tbl_cols, *tbl_vals]
    latex_table = tabulate([tbl_cols, *tbl_vals], tablefmt="latex", floatfmt=".3f", headers="firstrow")
    
    return str(latex_table)

# FIXME: look in scipy or numpry for a standard one or include this properly
# source: https://www.dabblingbadger.com/blog/2020/2/27/implementing-euclidean-distance-matrix-calculations-from-scratch-in-python
def distance_matrix(A, B, squared=False):
    """
    Compute all pairwise distances between vectors in A and B.

    Parameters
    ----------
    A : np.array
        shape should be (M, K)
    B : np.array
        shape should be (N, K)

    Returns
    -------
    D : np.array
        A matrix D of shape (M, N).  Each entry in D i,j represnets the
        distance between row i in A and row j in B.

    See also
    --------
    A more generalized version of the distance matrix is available from
    scipy (https://www.scipy.org) using scipy.spatial.distance_matrix,
    which also gives a choice for p-norm.
    """
    M = A.shape[0]
    N = B.shape[0]

    assert A.shape[1] == B.shape[1], f"The number of components for vectors in A \
        {A.shape[1]} does not match that of B {B.shape[1]}!"

    A_dots = (A*A).sum(axis=1).reshape((M,1))*np.ones(shape=(1,N))
    B_dots = (B*B).sum(axis=1)*np.ones(shape=(M,1))
    D_squared =  A_dots + B_dots -2*A.dot(B.T)

    if squared == False:
        zero_mask = np.less(D_squared, 0.0)
        D_squared[zero_mask] = 0.0
        return np.sqrt(D_squared)

    return D_squared
###

def coords(exp_data: ExperimentData):
    '''Returns a matrix with coordinates of the nodes ans well as the euclidean distance matrix.'''
    X = np.array([exp_data.initiator.position])
    for node in exp_data.neighbors:
        X = np.append(X, [node.position], axis=0)
    D = distance_matrix(X,X)

    return X, D