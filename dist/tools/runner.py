import argparse
import logging
import time
import os
import sys
import json
from contextlib import contextmanager
import re

from riotctrl.ctrl import RIOTCtrl
from riotctrl_shell.sys import Reboot

from utils.twr_shell import TwrActivity, TwrIfconfigParser
from utils.ble_shell import BleScan
from utils.iotlab import IoTLABExperiment

from utils.exp_data import *


IOTLAB_EXPERIMENT_DURATION = 120
DEFAULT_DIRECTORY = 'logs'
DEVNULL = open(os.devnull, 'w')

LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
LOG_LEVELS = ('debug', 'info', 'warning', 'error', 'fatal', 'critical')
LOGGER = logging.getLogger()

USAGE_EXAMPLE = '''example:
    python runner.py .
    python runner.py ../../riot-demo-apps
    python runner.py ../../riot-demo-apps --n 3 --d ../../riot-demo-apps/logs
    python runner.py ../../riot-demo-apps --iotlab-nodes dwm1001-1.saclay.iot-lab.info dwm1001-2.saclay.iot-lab.info
'''

PARSER = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter, epilog=USAGE_EXAMPLE)
PARSER.add_argument( 'app_dir', help='applications directory')
PARSER.add_argument('--log-directory', '--d', default=DEFAULT_DIRECTORY,
                    help='Logs directory')
PARSER.add_argument('--num-devices', '--n',type=int, default=5,
                     help='dwm1001 devices to use')
PARSER.add_argument('--iotlab-nodes', nargs='+', default=None,
                    help='List of iotlab-nodes network-addresses')
PARSER.add_argument('--loglevel','--l', choices=LOG_LEVELS, default='info',
                    help='Python logger log level')

UWB_RNG_APP = "01_uwb_rng"
BLE_SCAN_APP = "02_ble_scan_rss"


@contextmanager
def get_iotlab_experiment_nodes(boards: List[str], name: str='dwm1001-rng',
                                site: str='saclay') -> Tuple[IoTLABExperiment, List[Dict]]:
    """Start an experiment on IoT-LAB with boards

    :param boards: list of boards to book for experiment
        can be a list of RIOT 'BOARD', eg:
            ['samr21-xpro','samr21-xpro','iotlab-m3']
        or a list of IoT-LAB nodes 'network_address', eg:
            ['m3-1.grenoble.iot-lab.info', 'dwm1001-1.saclay.iot-lab.info']
    :param name: iotlab-experiment name
    :param site: iotlab-site, defaults 'saclay'
    """
    envs = list()
    # populate environments based on provided boards
    for board in boards:
        if IoTLABExperiment.valid_board(board):
            env = {'BOARD': '{}'.format(board)}
        else:
            env = {'IOTLAB_NODE': '{}'.format(board)}
        envs.append(env)
    exp = IoTLABExperiment(name=name, envs=envs, site=site)
    try:
        LOGGER.info("schedule experiment")
        exp.start(duration=IOTLAB_EXPERIMENT_DURATION)
        LOGGER.info("started experiment: {}".format(exp.exp_id))
        yield exp, envs
    finally:
        exp.stop()
        LOGGER.info("stopped experiment")


class TwrShell(Reboot, TwrActivity):
    """Convenience class inheriting from the Reboot and TwrActivity
    shell"""
    _netif = {
        'netif': None,
        'hwaddr': None,
        'hwaddr64': None,
        'panid': None,
    }


    def parse_netif(self):
        parser = TwrIfconfigParser()
        self._netif = parser.parse(self.ifconfig())


    def hwaddr(self):
        return self._netif['hwaddr']


    def netif(self):
        return self._netif['netif']


    def hwaddr64(self):
        return self._netif['hwaddr64']


    def panid(self):
        return self._netif['panid']


class BleScanShell(Reboot, BleScan):
    """Convenience class inheriting from the Reboot and BleScan shell"""
    pass


class RIOTCtrl_factory:
    def __init__(self, envs):
        self.envs = envs
        self.ctrls = list()


    def cleanup(self):
        """Close open terminals"""
        for ctrl in self.ctrls:
            ctrl.stop_term()
            self.ctrls.remove(ctrl)


    def getCtrl(self, app_dir, board=None, iotlab=None, modules=None, cflags=None):
        """Returns a RIOTCtrl with its terminal started, if no envs are
        available ot if no env matches the specified 'BOARD' nothing is
        returned

        :param board: riot 'BOARD' type to match in the class 'envs'
        :param app_dir: the application directory
        :param modules: extra modules to add to 'USEMODULE'
        :param cflags: optional 'CFLAGS'
        """
        if len(self.envs) == 0:
            LOGGER.error("no more envs available")
            return None
        if board:
            try:
                env = next(n for n in self.envs if n['BOARD'] == board)
            except:
                LOGGER.error("no envs with a  'BOARD': {}".format(board))
                return None
        elif iotlab:
            try:
                env = next(n for n in self.envs if n['IOTLAB_NODE'] == iotlab)
            except:
                LOGGER.error("no envs with a  'IOTLAB_NODE': {}".format(iotlab))
                return None
        else:
            env = self.envs[0]
        self.envs.remove(env)
        # add extra MODULES and CFLAGS, extend as needed
        if cflags:
            env['CFLAGS'] = cflags
        if modules:
            env['USEMODULE'] = modules
        # retrieve a RIOTCtrl Object
        node = RIOTCtrl(env=env, application_directory=app_dir)
        self.ctrls.append(node)
        # flash and start terminal
        LOGGER.info("\tflash {} to {}".format(
            os.path.basename(app_dir), node.env['IOTLAB_NODE']))
        assert(node.make_run(['flash'], stdout=DEVNULL, stderr=DEVNULL))
        assert(node.make_run(['reset'], stdout=DEVNULL, stderr=DEVNULL))
        # some boards need a delay to start
        time.sleep(1)
        node.start_term()
        # return node with started terminal
        return node


def create_directory(directory, clean=False, mode=0o755):
    """Directory creation helper with `clean` option.

    :param clean: tries deleting the directory before re-creating it
    """
    if clean:
        try:
            shutil.rmtree(directory)
        except OSError:
            pass
    os.makedirs(directory, mode=mode, exist_ok=True)


def create_and_dump(json_data, directory, name):
    """Create file with name 'name'.txt in 'directory' and dumps
       json_data do file
    """
    file_name = '{}.txt'.format(name)
    LOGGER.info("logging to file {}".format(file_name))
    file_path = os.path.join(directory, file_name)
    if os.path.exists(file_path):
        LOGGER.warning(f"File {file_name} already exists, overwriting")
    try:
        with open(file_path, 'w') as f:
            f.write('{}\n'.format(json_data))
    except OSError as err:
        sys.exit('Failed to create a log file: {}'.format(err))
    return file_path


def twr_range_parse(data, exp_data, key):
    """
    """
    measurement = exp_data.measurements[key]
    for line in data.splitlines():
        try:
            data = TwrData.from_json_str(line)
            measurement.uwb_dist.append(data.raz[0])
            rss_data = RssMeasurement(tx_power=None, rss=data.rssi)
            measurement.uwb_rss_data.append(rss_data)
        except ValueError:
            LOGGER.debug("JSONDecodeError on: '{}'".format(line))


def perform_twr_range(tag, anchor, alg='ss', runtime=10e3):
    """Starts ranging activity between: tag->anchor using algorithm
       'alg' for 'time'

    :param alg: twr algo 'ss','ss-ack','ss-ext','ds,'ds-ack'
    :param runtime: runtime in ms
    """
    # reboot devices
    tag.reboot()
    anchor.reboot()
    # set role
    tag.twr_set('role', 'tag')
    anchor.twr_set('role', 'anchor')
    # set algorithm
    tag.twr_set('alg', alg)
    anchor.twr_set('alg', alg)
    timeout = 1.1*runtime/1e3 + 10
    return tag.twr_start(anchor.hwaddr(), runtime, timeout=timeout)


def twr_range(exp_data, exp_envs, app_dir):
    """
    """
    app_dir = os.path.join(app_dir, UWB_RNG_APP)
    # get a factory object
    factory = RIOTCtrl_factory(exp_envs)
    try:
        LOGGER.info("bootsraping uwb devices")
        # get a Twr shell interaction for every node in the experiment
        tag = factory.getCtrl(app_dir, iotlab=exp_data.initiator.network_address)
        tag_shell = TwrShell(tag)
        tag_shell.parse_netif()
        anchors = [factory.getCtrl(app_dir, iotlab=neigh.network_address) for neigh in exp_data.neighbors]
        LOGGER.info("start twr ranging")
        for anchor in anchors:
            anchor_nwk_addr = anchor.env['IOTLAB_NODE']
            LOGGER.info("\t{} range -> {}".format(
                tag.env['IOTLAB_NODE'], anchor_nwk_addr))
            anchor_shell = TwrShell(anchor)
            anchor_shell.parse_netif()
            data = perform_twr_range(tag_shell, anchor_shell)
            anchor_hwaddr = anchor_shell.hwaddr()
            exp_data.measurements[anchor_nwk_addr].uwb_address = anchor_hwaddr
            twr_range_parse(data, exp_data, anchor_nwk_addr)
    finally:
        factory.cleanup()


def ble_scan_parse(data, exp_data):
    """
    """
    for line in data.splitlines():
        try:
            scan_data = ScanData.from_json_str(line)
            for node in scan_data.nodes:
                network_address = '{}.saclay.iot-lab.info'.format(node.name)
                if network_address in exp_data.measurements.keys():
                    rss_data = RssMeasurement(tx_power=node.txpwr_dbm, rss=node.last_rssi)
                    exp_data.measurements[network_address].ble_rss_data.append(rss_data)
                    exp_data.measurements[network_address].ble_address = node.mac_address()
        except ValueError:
            LOGGER.debug("JSONDecodeError on: '{}'".format(line))


def perform_ble_scan(scanner, advertisers, scan_time=0.5e3, cycles=100):
    """
    """
    # reboot, set name and start advertising
    for node in advertisers:
        node_shell = BleScanShell(node)
        # There seems to be some kind of bug beause the first `>` is missed
        node_shell.reboot()
        reg = r'([0-9a-zA-Z\-]+-\d+)\.[a-z]+\.iot-lab\.info'
        match = re.search(reg, node.env['IOTLAB_NODE'])
        node_shell.autoadv_name(match.group(1))
        node_shell.autoadv_start()
    # reboot and set name
    scanner_shell = BleScanShell(scanner)
    scanner_shell.reboot()
    time.sleep(1)
    # set timeout (in seconds) based on scan_time + marging for printing JSON
    timeout = scan_time/1e3 + 3
    output = ''
    LOGGER.info("start ble scan")
    LOGGER.debug("\tscan time: {}".format(scan_time))
    LOGGER.debug("\tcycles:    {}".format(cycles))
    for i in range(cycles):
        LOGGER.info("\t{}: scan cycle {}".format(scanner.env['IOTLAB_NODE'], i))
        output += scanner_shell.scan(scan_time, 1, timeout=timeout)
    return output


def ble_scan(exp_data, exp_envs, app_dir):
    """
    """
    app_dir = os.path.join(app_dir, BLE_SCAN_APP)
    # get a factory object
    factory = RIOTCtrl_factory(exp_envs)
    try:
        # get a Twr shell interaction for every node in the experiment
        LOGGER.info("bootsraping ble devices")
        scanner = factory.getCtrl(app_dir, iotlab=exp_data.initiator.network_address)
        advertisers = [factory.getCtrl(app_dir, iotlab=neigh.network_address) for neigh in exp_data.neighbors]
        data = perform_ble_scan(scanner, advertisers)
        ble_scan_parse(data, exp_data)
    finally:
        factory.cleanup()


def run(devices, app_dir, directory, initiator_network_address: str = None):
    """
    """
    exp_data = None
    # start iotlab experiment and recover list of nodes
    with get_iotlab_experiment_nodes(devices) as (exp, exp_envs):
        # get list of ExperimentNodes and set initiator
        neighbors = list()
        LOGGER.info((f'>>> Experiment nodes:\n {exp.get_nodes_position()}'))
        for node in exp.get_nodes_position():
            neighbors.append(from_dict(data_class=ExperimentNode, data=node))
        # set initiator if provided
        if not initiator_network_address:
            initiator = neighbors[0] ## default to first element
        else:
            for node in neighbors:
                if node.network_address == initiator_network_address:
                    initiator = node
                    break
        print(f'setting initiator to {initiator_network_address}')                
        neighbors.remove(initiator)
        # create empty measurements dict
        measurements = dict()
        for node in neighbors:
            empty_data = MeasurementData('', list(), '', list(), list())
            measurements.update({ node.network_address: empty_data })
        exp_data = ExperimentData(initiator, neighbors, measurements)
        # perform ranging and populate according measurements
        twr_range(exp_data, exp_envs.copy(), app_dir)
        # perform ble scan
        ble_scan(exp_data, exp_envs.copy(), app_dir)
        # dump json
        create_and_dump(exp_data.to_json_str(), directory, 'json_dump')
    return exp_data


def main(args=None):
    args = PARSER.parse_args()

    # setup logger
    if args.loglevel:
        loglevel = logging.getLevelName(args.loglevel.upper())
        LOGGER.setLevel(loglevel)

    LOGGER.addHandler(LOG_HANDLER)
    LOGGER.propagate = False

    # parse args
    app_dir = args.app_dir
    log_directory = args.log_directory
    num_devices = args.num_devices
    iotlab_nodes = args.iotlab_nodes
    # create directory if non existant
    create_directory(log_directory)

    # setup experiments and run
    # FIXME hardcoded, externalize and review app config to simplify
    iotlab_nodes = [f'dwm1001-{i+1}.saclay.iot-lab.info' for i in reversed(range(5))]
    iotlab_nodes.extend([f'dwm1001-{i+6}.saclay.iot-lab.info' for i in range(5)])
    print(iotlab_nodes)
    if iotlab_nodes:
        devices = iotlab_nodes
        initiator_network_address = iotlab_nodes[0]
    else:
        initiator_network_address=None
        devices = ['dwm1001'] * num_devices
    exp_data = run(devices, app_dir, log_directory,initiator_network_address), 


if __name__ == "__main__":
    main()
