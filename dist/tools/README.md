### Requirements

To use `runner.py` you need to install [`riotctrl`][riotctrl] and
[`iotlabcli`][iotlabcli] python packages among others:

```sh
pip install -r requirements.txt
```

Furthermore the `PYTHONPATH` needs to include the `pythonlibs` of RIOT:

```sh
export PYTHONPATH=${RIOTBASE}/dist/pythonlibs:${PYTHONPATH}
```

### Usage

usage: runner.py [-h] [--log-directory LOG_DIRECTORY] [--num-devices NUM_DEVICES]
                 [--loglevel {debug,info,warning,error,fatal,critical}]
                 app_dir

positional arguments:
  app_dir               applications directory

optional arguments:
  -h, --help            show this help message and exit
  --log-directory LOG_DIRECTORY, --d LOG_DIRECTORY
                        Logs directory
  --num-devices NUM_DEVICES, --n NUM_DEVICES
                        dwm1001 devices to use
  --loglevel {debug,info,warning,error,fatal,critical}, --l {debug,info,warning,error,fatal,critical}
                        Python logger log level

example:
    python runner.py .
    python runner.py ../../01_uwb_rng
    python runner.py ../../01_uwb_rng --n 3 --d ../../01_uwb_rng/logs
