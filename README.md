# riot-demo-apps

[![pipeline status](https://gitlab.inria.fr/pepper/riot-demo-apps/badges/master/pipeline.svg)](https://gitlab.inria.fr/pepper/riot-demo-apps/-/commits/master)

A repo for centralizing reference demos around BLE and UWB proximity tracing based on the [DMW1001-DEV](https://www.decawave.com/product/dwm1001-development-board/) boards which embeds the certified [DMW1001](https://www.decawave.com/product/dwm1001-module/) radio module (MCU+BLE+UWB+antennae).

## Some documentation refs
- Intro to BLE: This free [ebook](https://www.novelbits.io/introduction-to-bluetooth-low-energy-book/) provides a nice introduction on BLE;
- The Nimble stack: an open source [alternative](https://mynewt.apache.org/latest/network/index.html) to Nordic's [SoftDevice](https://www.nordicsemi.com/Software-and-tools/Software/S132) BLE stack.
- Nimble and RIOT: [slides](http://summit.riot-os.org/2018/wp-content/uploads/sites/10/2018/09/1_1-Szymon-Janc-NimBLE.pdf) providing an overview of nimble port in riot

## Preparing the board
In order to mass erase the flash (factory reset) and configure the reset pin (the UICR register) correcly run the [RIOT/dist/tools/nrf52_resetpin_cfg](RIOT/dist/tools/nrf52_resetpin_cfg) app on a board prior using it wih RIOT.
```shell
$ cd RIOT/dist/tools/nrf52_resetpin_cfg
$ JLINK_PRE_FLASH='erase' BOARD=dwm1001 DOCKER_ENV_VARS=RESET_PIN RESET_PIN=21 BUILD_IN_DOCKER=1 make flash
```
Another way of doing this, if you have Nordic's [nRF tools](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_nrf5x_cltools%2FUG%2Fcltools%2Fnrf5x_nrfjprogexe_reference.html) installed, is by running
```shell
$ nrfjprog --pinresetenable
```

## Some interesting apps: nimble in RIOT
Based on the nimble package, three examples are provided in RIOT:
- [RIOT/examples/nimble_scanner](./RIOT/examples/nimble_scanner): A passive ble scanner app with a `scan` shell command showing advertising ble nodes in the neighborhood
- [RIOT/examples/nimble_gatt](./RIOT/examples/nimble_gatt): A GATT server illustrating how to advertise data and exposes R/W services. It can be tested using a mobile app [nRF Connect](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-mobile) available on Android and iOS.
- [RIOT/examples/nimble_heart_rate_sensor](./RIOT/examples/nimble_heart_rate_sensor): A heartbeat BLE sensor demo that illustrates data transfer amon basic services (Device information and device battery level).

## Current apps
The following apps are in process of being available:
- [01_uwb_rng](./01_uwb_rng): a shell app on the usage of uwb ranging
- [02_ble_scan_rss](./02_ble_scan_rss): a shell app on the usage of BLE RSS scan
- [03_ble_rng_gatt](./03_ble_rng_gatt): a complete app exposing UWB ranging as GATT server
- [04_ble_rng_gap](./04_ble_rng_gap): a complete app exposing UWB ranging using GAP scan request/response mechanism

## Initializing the repository:

RIOT is included as a submodule of this repository. We provide a `make` helper
target to initialize it.
From the root of this repository, issue the following command:

```
$ make init-submodules
```

### Building the firmwares:

From the root directory of this repository, simply issue the following command:

```
$ make
```

### Flashing the firmwares

For each firmware use the RIOT way of flashing them. For example, in
`01_uwb_rng`, use:

```
$ make -C 1_uwb_rng flash
```
to flash the firmware on a `dwm1001` `BOARD`, if another `BOARD` is the target specify it in the command as:

```
$ BOARD=<BOARD> make -C 1_uwb_rng flash
```

### Global cleanup of the generated firmwares

From the root directory of this repository, issue the following command:

```
$ make clean
```
