BLE GAP-based connection-less uwb ranging
====
The idea of this app is to expose the UWB ranging capapibilities of the node in the GAP advertisement packet.
Opposed to the connection-based approach (as in [../02_ble_rng_gatt](../02_ble_rng_gatt)), we use the following GAP features:
- advertisement data: expose uwb rng feratures in the pdu (manufacturer specific fields)
- scan request/response: use the pdu of scan request/response to initiate an uwb ranging for a window of time. The initiator will act as a uwb tag whils the responder will act as uwb anchor

TODO validate fasibilty and show how to run a complete demo with screenshots.
