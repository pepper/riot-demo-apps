import json
from dataclasses import dataclass, asdict
from typing import List, Dict
import sys
import re

from dacite import from_dict
import matplotlib.pyplot as plt


@dataclass
class Node:
    addr: List[str]
    addr_type: int
    name: str
    adv_type: int
    ad_data: List[str]
    last_rssi: int
    txpwr_dbm: int
    adv_msg_cnt: int
    first_update: int
    last_update: int

    def mac_address(self) -> str:
        return ''.join(self.addr)
    
    def adv_int(self) -> int:
        return (self.last_update - self.first_update) / self.adv_msg_cnt


@dataclass
class ScanData:
    nodes: List[Node]

    def to_dict(self) -> Dict[str, Node]:
        mac_addr = map(lambda node: node.mac_address(), self.nodes)
        return dict(zip(mac_addr, self.nodes))

    def rss_data(self) -> Dict[str, int]:
        mac_addr = map(lambda node: node.mac_address(), self.nodes)
        rssi = map(lambda node: node.last_rssi, self.nodes)
        return dict(zip(mac_addr, rssi))

    def to_json_str(self) -> str:
        json_dict = asdict(self)
        return json.dumps(json_dict)

    @staticmethod
    def from_json_str(json_string: str):
        json_dict = json.loads(json_string)
        return from_dict(data_class=ScanData, data=json_dict)


def parse_rss_captures(capture_filename) -> Dict[str, int]:
    captures = dict()

    def append_rss_data(node: Node):
        mac_addr = node.mac_address()
        if mac_addr not in captures:
            captures[mac_addr] = list()
        captures[mac_addr].append(node.last_rssi)

    with open(capture_filename, "r") as file:
        for line in file:
            scan_data = ScanData.from_json_str(line)
            for node in scan_data.nodes:
                append_rss_data(node)

    return captures


def fmt_macc_addr(addr: str) -> str:
    return ':'.join(re.findall('..', addr))


def print_stats(rss_data: Dict[str, int]):
    print(f'Parsed {len(rss_data)} nodes: {list(rss_data.keys())}')
    for node in rss_data.keys():
        print(f'\t{node}: {len(rss_data[node])} samples')


def plot_rss_timeseries(rss_data: Dict[str, int]):
    n = len(rss_data)
    i = 0
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True, squeeze=False)
    for node in rss_data:
        axs[i,0].plot(rss_data[node], label=fmt_macc_addr(node))
        axs[i,0].legend()
        axs[i,0].set_ylabel('RSSI (dBm)')
        axs[i,0].grid(True)
        i = i+1
    axs[i-1,0].set_xlabel('sample')
    fig.suptitle('RSSI timeseries')


def plot_rss_timeseries_fig(rss_data: Dict[str, int]):
    plt.figure()
    for node in rss_data:
        plt.plot(rss_data[node], label=fmt_macc_addr(node))
    plt.legend(loc='upper right', borderaxespad=0., shadow=True)
    plt.title('Captured RSSI values per node')
    plt.xlabel('sample')
    plt.ylabel('RSSI (dBm)')
    plt.grid(True)

    plt.tight_layout()

def plot_rss_histograms(rss_data: Dict[str, int]):
    n = len(rss_data)
    i = 0
    fig, axs = plt.subplots(n, 1, sharey=True, sharex=True, tight_layout=True, squeeze=False)
    for node in rss_data:
        axs[i,0].hist(rss_data[node], label=fmt_macc_addr(node))
        axs[i,0].legend()
        axs[i,0].set_ylabel('frequency')
        i = i+1
    axs[i-1,0].set_xlabel('RSSI (dBm')
    fig.suptitle('RSSI distributions')

def plot_rss_histograms_fig(rss_data: Dict[str, int]):
    plt.figure()
    for node in rss_data:
        plt.hist(rss_data[node], label=fmt_macc_addr(node))
    plt.legend(borderaxespad=0., shadow=True)
    plt.title('RSSI distribution per node')
    plt.xlabel('RSSI (dBm)')
    plt.ylabel('frequency')

    plt.tight_layout()
    
def main(capture_filename):
    print(f'Parsing log_file={capture_filename}')
    captures = parse_rss_captures(capture_filename)
    print_stats(captures)
    plot_rss_timeseries(captures)
    plot_rss_timeseries_fig(captures)
    plot_rss_histograms(captures)
    plot_rss_histograms_fig(captures)
    plt.show(block=False)
    input('Press enter to exit')


if __name__ == "__main__":
    main(capture_filename=sys.argv[1])
