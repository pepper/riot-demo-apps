Scan BLE advertisement paquets
==============================

This showcases a simple BLE scan application that dumps rssi information
in json or text format.

A detailed walkthrough will leverage this application to perform simple
BLE scan between 3 devices on the [FIT IoT-LAB](https://www.iot-lab.info/)
testbed, this can also be ran on local hardware if available.

Table of contents:

- [Prerequisites][prerequisites]
- [Walkthrough][walkthrough]
- [About This Application][about]

### Prerequisites
[prerequisites]: #rerequisites

1. Install iotlab-cli tools

    - pip install iotlab-cli

2. Create an iotlab account

    - https://www.iot-lab.info/testbed/signup

3. Authenticate:

```shell
$ iotlab-auth --user <username> --password <password>
```

4. OPTIONAL: if using the [parse_plot.py](plot_demo/parse_plot.py)
script then install the required python libraries:

    - pip install -r plot_demo/requirements.txt

### Walkthrough
[walkthrough]: #Walkthrough

In this exercise, you will need RIOT boards supporting BLE (see
[supported boards](https://www.iot-lab.info/docs/boards/overview/) if
running on IoT-LAB). Here we will use one `dwm1001` board to scan
BLE advertisement paquets sent by 2 other `dwm1001` boards.

BLE support in RIOT is provided by the [external package Nimble](http://doc.riot-os.org/group__pkg__nimble.html)
which is an adaption of [Apache Nimble](https://github.com/apache/mynewt-nimble)
to RIOT and provides major BLE features.

In this application, you will:
- build an application with shell command to start a BLE scan and to
  send BLE advertise paquets
- retrieve the scan output and store it in a file
- plot RSSI indicator of the received paquets

#### Launch an Experiment

1. Submit an experiment with 3 dwm1001 boards and during 60 minutes:

```shell
$ iotlab-experiment submit -n "riot-ble-scan" -d 60 -l 3,archi=dwm1001:dw1000+site=saclay
```

2. Wait for the experiment to be in the Running state:

```shell
$ iotlab-experiment wait --timeout 30 --cancel-on-timeout
```

**Note:** If the command above returns the message
`Timeout reached, cancelling experiment <exp_id>`, try to re-submit your
experiment later or try using a different BLE capable board.

3. Get the experiment nodes list:

``` bash
$ iotlab-experiment get --nodes
{
    "items": [
        ...
        {
            "archi": "dwm1001:dw1000",
            "camera": "0",
            "mobile": "0",
            "mobility_type": " ",
            "network_address": "dwm1001-2.saclay.iot-lab.info",
            "production": "YES",
            "site": "saclay",
            "state": "Alive",
            "uid": " ",
            "x": "4.42",
            "y": "68.3",
            "z": "6"
        },
        ...
    ]
}
```

Take note of each node's `"network_address"`, we will use this to
connect to specific devices. The `"network_address"` follows the following
structure `<node-name>-<id>.saclay.iot-lab.info`. For the rest of this
walkthrough every time you see `<node>` replace with the
matching `"network_address"`

### Build the RIOT firmware

```shell
$ make
```

Now you can flash the firmware on the 3 nodes of the experiment:

```shell
$ iotlab-node --flash bin/dwm1001/ble_scan_rss.bin
```

Or with make (repeating the command adjusting the `"network_address"`):

```shell
$ IOTLAB_NODE=<node> make flash
```

### Play with the RIOT firmware

Let's start by connecting to the first board that will be used later
to scan the BLE advertisement paquets.

1. Open a serial terminal on the first board. Note that it will output
nothing by default, you have to enter commands (see the next steps).

```shell
$ TERMLOG=$(pwd)/plot_demo/scan_raw.log make IOTLAB_NODE=<node> term
```

_Note:_  in the command above, we use the `TERMLOG` variable with a file
path. This is a RIOT build system trick that will dump all RIOT shell
output to a log file. We will use this file later to parse the scanned
BLE packets.

2. In the RIOT shell opened by the previous commands, you can check the
available commands:

```shell
> help
help
Command              Description
---------------------------------------
scan                 trigger a BLE scan
autoadv              manages BLE advertisment
```

Keep this terminal open, we will come back to it later to start the scan.

3. Open a serial terminal on the 2 remaining boards (repeat twice the
following command, adapted with the node's `"network_address"`):

```shell
$ make IOTLAB_NODE=<node> term
```

4. In each of these 2 RIOT shells, set the name of the board (`<node>-<id>`)
and start the auto-advertisement:

```shell
> autoadv name <node>-<id>
autoadv name <node>-<id>
> autoadv name
autoadv name
Current name = <node>-<id>
> autoadv start
```

5. Now go back to the first RIOT shell and start 100 BLE scan of 100
milliseconds each, we also configure an output in json format so that we
can parse later:

```shell
> scan
> scan
scan
usage: scan <output_type> <scan_timeout> <scan_cycles>
- output_type: json or txt for log output
- scan_timeout: scan timeout in milliseconds
- scan_cycles: the number of scan cycles

> scan json 100 100
{"nodes": [{"addr":["CC","3E","19","71","63","70"],"addr_type":1,"name":"dwm1001-2","adv_type":0,"ad_data":["02","01","06","0A","08","64","77","6D","31","30","30","31","2D","32","02","0A","00"],"last_rssi":-53,"txpwr_dbm":0,"adv_msg_cnt":826,"first_update":1115111832,"last_update":1145030423}, {"addr":["DB","DD","06","5A","89","E8"],"addr_type":1,"name":"dwm1001-3","adv_type":0,"ad_data":["02","01","06","0A","08","64","77","6D","31","30","30","31","2D","33","02","0A","00"],"last_rssi":-64,"txpwr_dbm":0,"adv_msg_cnt":852,"first_update":1115096137,"last_update":1144961615}]}
...
```

The above output shows the result of a single scan cycle where information
on each scanned device is showed.

### Free up the resources

You are done with the experiment, so stop your experiment to free up the
devices:

``` bash
$ iotlab-experiment stop
```

### Process the log file

If everything went well, you should have a [scan_raw.log](scan_raw.log)
in the `plot_demo` directory.

``` bash
    $ head -n 10 plot_demo/scan_raw.log
```

As you can see, this file contains all the output cached by the scanning
node, including the shell input commands.

To parse the output you can use the helper script in
[plot_demo/parser.py](plot_demo/parser.py)

```shell
$ python plot_demo/parse_plot.py plot_demo/scan_raw.log
```

You will get some figures similar to:

-![](plot_demo/pics/Figure_1.png)
-
-![](plot_demo/pics/Figure_2.png)
-
-![](plot_demo/pics/Figure_3.png)
-
-![](plot_demo/pics/Figure_4.png)

Congratulations! You completed this BLE scan walkthrough with success.

### About This Application
[about]: #About-This-Application

The application provides two shell commands to perform BLE scans:

- `scan`: to trigger a BLE scan
- `autoadv`: to configure and set nodes to advertise

### `scan` cmd

```shell
Command              Description
---------------------------------------
scan                 trigger a BLE scan

usage: scan <output_type> <scan_timeout> <scan_cycles>
 - output_type: json or txt for log output
 - scan_timeout: scan timeout in milliseconds
 - scan_cycles: the number of scan cycles
```

The scan command allows to trigger a node to scan for BLE advertisements
packets. The device will scan for `<scan_timeout>` in milliseconds and
then report the scanned devices and some information on that device.
This can be repeater over a number of `<scan_cycles>`. The output can
be in **txt** or **json** format.


Usage examples:

- Single scan with **txt** output:

```shell
> scan txt 1000 1
[ 0] e3:4e:53:e9:30:5a (RANDOM) [IND] "node.1", adv_msg_cnt: 29, adv_int: 31773us, last_rssi: -71
[ 1] cf:9a:25:1a:a0:b4 (RANDOM) [IND] "node.2", adv_msg_cnt: 28, adv_int: 34400us, last_rssi: -48
```

- Mutltiple scan cycles with **txt** ouput:

```shell
> scan txt 1000 2
[ 0] e3:4e:53:e9:30:5a (RANDOM) [IND] "node.1", adv_msg_cnt: 28, adv_int: 30164us, last_rssi: -69
[ 1] cf:9a:25:1a:a0:b4 (RANDOM) [IND] "node.2", adv_msg_cnt: 27, adv_int: 33142us, last_rssi: -46
[ 0] e3:4e:53:e9:30:5a (RANDOM) [IND] "node.1", adv_msg_cnt: 32, adv_int: 30067us, last_rssi: -68
[ 1] cf:9a:25:1a:a0:b4 (RANDOM) [IND] "node.2", adv_msg_cnt: 28, adv_int: 33253us, last_rssi: -46
```

- Single scan with **json** output:
```shell
> scan json 1000 1
{"nodes": [{"addr":["CF","9A","25","1A","A0","B4"],"addr_type":1,"name":"node.2","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","32","02","0A","00"],"last_rssi":-46,"txpwr_dbm":0,"adv_msg_cnt":26,"first_update":967350512,"last_update":968209493}, {"addr":["E3","4E","53","E9","30","5A"],"addr_type":1,"name":"node.1","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","31","02","0A","00"],"last_rssi":-66,"txpwr_dbm":0,"adv_msg_cnt":23,"first_update":967345031,"last_update":968257799}]}
```

- Mutltiple scan with **json** ouput:
```shell
> scan json 1000 2
{"nodes": [{"addr":["E3","4E","53","E9","30","5A"],"addr_type":1,"name":"node.1","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","31","02","0A","00"],"last_rssi":-72,"txpwr_dbm":0,"adv_msg_cnt":20,"first_update":1010150247,"last_update":1011000273}, {"addr":["CF","9A","25","1A","A0","B4"],"addr_type":1,"name":"node.2","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","32","02","0A","00"],"last_rssi":-48,"txpwr_dbm":0,"adv_msg_cnt":24,"first_update":1010088196,"last_update":1010991570}]}
{"nodes": [{"addr":["E3","4E","53","E9","30","5A"],"addr_type":1,"name":"node.1","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","31","02","0A","00"],"last_rssi":-66,"txpwr_dbm":0,"adv_msg_cnt":24,"first_update":1011252996,"last_update":1011973757}, {"addr":["CF","9A","25","1A","A0","B4"],"addr_type":1,"name":"node.2","adv_type":0,"ad_data":["02","01","06","07","08","6E","6F","64","65","2E","32","02","0A","00"],"last_rssi":-47,"txpwr_dbm":0,"adv_msg_cnt":32,"first_update":1011128044,"last_update":1012089963}]}
```

## `autoadv` cmd

```shell
Command              Description
---------------------------------------
autoadv              manages BLE advertisment

usage: autoadv <start|stop|name|pwr> [<adv_duration_ms>|<device_name>]
 - start <adv_timeout_ms>: sets advertisement duration in ms
 - name <device_name>: sets the advertised device name
 - stop: stops BLE advertisement
 - pwr: prints current Tx Power from phy
````

Based on the autoadv module, the app also provides a facility to advertise
data on a regular basis, thus generating BLE traffic for rssi measurements.
The advertised data contain the transmitted Tx Power level in dBm and the
device name.

This is useful when flashing multiple nodes at different positions where
they all scan and advertise simultaneously.

Usage examples:

- set/get device name

```shell
> autoadv name
Current name = nimble
> autoadv name Paco
> autoadv name
Current name = Paco
```

- Start advertisement
```shell
> autoadv start
```

- Stop advertisement
```shell
> autoadv stop
```
